using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a basic agent.
/// </summary>
[RequireComponent(typeof(SensorySystem))]
public class Agent : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	
	public float ReactionTime = 1.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	
	protected SensorySystem sensors;
	protected Action[] actions;
	protected Action currentAction;

	#endregion

	#region PRIVATE VARIABLES

	private float reactionTimer = 0.0f;

	#endregion

	#region PROPERTIES

	/// <summary>
	/// Gets the sensors.
	/// </summary>
	/// <value>The sensors.</value>
	public SensorySystem Sensors
	{
		get { return sensors;	}
	}

	/// <summary>
	/// Gets the current action.
	/// </summary>
	/// <value>The current action.</value>
	public Action CurrentAction
	{
		get { return currentAction;	}
	}

	/// <summary>
	/// Gets all actions.
	/// </summary>
	/// <value>The actions.</value>
	public Action[] Actions
	{
		get { return actions;	}
	}
	
	#endregion



	#region MONOBEHAVIOUR

	protected void Start()
	{
		// Setup reaction timer
		reactionTimer = ReactionTime;

		// Get sensory system
		sensors = GetComponent<SensorySystem>();
		if(sensors == null)
			Debug.LogWarning(this.name + " - Agent: Unable to locate sensory system on game object.");

		// Get all available actions
		actions = GetComponents<Action>();
		/*
		if(actions.Length > 0)
			SwitchAction(actions[0]);
		else
		*/
		if(actions == null || actions.Length == 0)
			Debug.LogWarning(this.name + " - Agent: Unable to locate actions on game object.");
	}

	void Update()
	{
		// Update sensors
		Sense();

		// Update reaction timer
		reactionTimer -= Time.deltaTime;
		if(reactionTimer <= 0.0f)
		{
			reactionTimer = ReactionTime;
			// Think if reaction time is up
			Think();
		}

		// Execute current action
		Act();
	}

	void OnDestroy()
	{
		// Clean the currently active action up before destruction.
		if(currentAction != null)
			currentAction.CleanUp();
	}

	#endregion



	#region PUBLIC METHODS

	/// <summary>
	/// Returns all actions which are possible in the current situation.
	/// </summary>
	/// <returns>The possible actions.</returns>
	public List<Action> GetPossibleActions()
	{
		// Get possible actions
		List<Action> possible = new List<Action>();
		// Add possible actions according to sensors
		for(int i = 0; i < actions.Length; i++)
			if(actions[i].CheckRequirements(sensors.Memory))
				possible.Add(actions[i]);
		
		return possible;
	}

	#endregion


	
	#region PROTECTED METHODS

	/// <summary>
	/// Returns all actions available for execution.
	/// </summary>
	/// <returns>The available actions.</returns>
	virtual protected List<Action> GetAvailableActions()
	{
		// Simply return all possible actions
		return GetPossibleActions();
	}

	#endregion


	
	#region PRIVATE METHODS

	/// <summary>
	/// Method representing the observation phase.
	/// </summary>
	private void Sense()
	{
		if(sensors != null)
			sensors.GatherInformation();
	}

	/// <summary>
	/// Method representing the thought phase.
	/// </summary>
	private void Think()
	{
		SelectAction(GetAvailableActions());
	}

	/// <summary>
	/// Method representing the acting phase.
	/// </summary>
	private void Act()
	{
		if(currentAction != null)
			currentAction.Execute();
	}

	/// <summary>
	/// Selects an action for execution from a list of possible actions.
	/// </summary>
	/// <param name="_Possible">The possible actions.</param>
	private void SelectAction(List<Action> _Possible)
	{
		if(_Possible != null && _Possible.Count > 0)
		{
			Action next = _Possible[Random.Range(0, _Possible.Count)];
			if(next != currentAction)
				SwitchAction(next);
		}
	}

	/// <summary>
	/// Switches to the specified action, cleanly exiting the old one.
	/// </summary>
	/// <param name="_Action">The new action.</param>
	private void SwitchAction(Action _Action)
	{
		// Clean up old action
		if(currentAction != null)
			currentAction.CleanUp();
		
		currentAction = _Action;

		// Prepare new action
		if(currentAction != null)
			currentAction.Prepare();
	}

	#endregion
}

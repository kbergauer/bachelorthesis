using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a personality-driven agent.
/// </summary>
public class PersonalityAgent : Agent 
{
	#region PUBLIC VARIABLES

	[SerializeField]
	public Personality Factors;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES
	#endregion



	#region MONOBEHAVIOUR

	new protected void Start()
	{
		base.Start();

		foreach(Action a in actions)
			CalculateRawAffinity(a);
	}

	#endregion



	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PROTECTED METHODS

	/// <summary>
	/// Returns all actions available for execution.
	/// </summary>
	/// <returns>The available actions.</returns>
	override protected List<Action> GetAvailableActions()
	{
		// Get all possible actions
		List<Action> possible = base.GetAvailableActions();
		
		// Calculate action affinities
		for(int i = 0; i < possible.Count; i++)
			possible[i].CurrentAffinity = possible[i].GetAffinity(sensors.Memory);
		
		// Sort list by affinity
		possible.Sort();
		
		// Remove all actions with a lower affinity than the highest
		for(int i = 0; i < possible.Count; i++)
		{
			if(possible[i].CurrentAffinity < possible[0].CurrentAffinity)
			{
				possible.RemoveAt(i);
				i--;
			}
		}
		
		// Return selected actions
		return possible;
	}

	#endregion



	#region PRIVATE METHODS

	/// <summary>
	/// Calculates the agent's raw affinity of executing the specified action.
	/// </summary>
	/// <param name="_Action">The action to calculate the affinity for.</param>
	private void CalculateRawAffinity(Action _Action)
	{
		float sum = 0.0f;
		sum += Mathf.Abs(Factors.Neuroticism - _Action.Weights.Neuroticism);
		sum += Mathf.Abs(Factors.Extroversion - _Action.Weights.Extroversion);
		sum += Mathf.Abs(Factors.Openness - _Action.Weights.Openness);
		sum += Mathf.Abs(Factors.Agreeableness - _Action.Weights.Agreeableness);
		sum += Mathf.Abs(Factors.Conscientiousness - _Action.Weights.Conscientiousness);

		_Action.RawAffinity = 1 - (sum / 5.0f);
	}

	#endregion
}

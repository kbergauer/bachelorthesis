﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class holding a personality's primary factors.
/// </summary>
[System.Serializable]
public class Personality
{
	[Range(0.0f, 1.0f)]
	public float Neuroticism;
	[Range(0.0f, 1.0f)]
	public float Extroversion;
	[Range(0.0f, 1.0f)]
	public float Openness;
	[Range(0.0f, 1.0f)]
	public float Agreeableness;
	[Range(0.0f, 1.0f)]
	public float Conscientiousness;
}

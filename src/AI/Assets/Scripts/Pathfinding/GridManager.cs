﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Singleton class representing the grid manager for pathfinding.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class GridManager : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public int Rows;
	public int Columns;
	public float CellSize;
	public bool ShowGrid = true;
	public Color GridColor = Color.blue;
	public bool ShowObstacleBlocks = true;
	public Color ObstacleColor = Color.red;
	public bool ShowOrigin = true;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES

	private static GridManager instance = null;

	private Vector3 origin = new Vector3();
	private GameObject[] obstacleList;

	#endregion

	#region PROPERTIES

	/// <summary>
	/// Gets the current instance of the GridManager.
	/// </summary>
	/// <value>The instance.</value>
	public static GridManager Instance
	{
		get
		{
			if(instance == null)
			{
				instance = FindObjectOfType(typeof(GridManager)) as GridManager;
				//if(instance == null)
					//Debug.LogWarning("GridManager: Unable to locate a GridManager object. You need to have exactly one GridManager in the scene.");
			}
			
			return instance;
		}
	}

	/// <summary>
	/// Gets or sets the nodes representing the grid.
	/// </summary>
	/// <value>The nodes.</value>
	public Node[,] Nodes
	{
		get;
		set;
	}

	/// <summary>
	/// Gets the grid's origin.
	/// </summary>
	/// <value>The origin.</value>
	public Vector3 Origin
	{
		get { return origin;	}
	}

	#endregion



	#region MONOBEHAVIOUR

	void Awake()
	{
		obstacleList = GameObject.FindGameObjectsWithTag("Obstacle");
		CalculateObstacles();
	}

	void OnDrawGizmos()
	{
		if(ShowGrid)
			DebugDrawGrid(transform.position, Rows, Columns, CellSize, GridColor);

		if(ShowOrigin)
			Gizmos.DrawSphere(transform.position, 0.5f);

		if(ShowObstacleBlocks)
		{
			Vector3 cubicCellSize = new Vector3(CellSize, 1.0f, CellSize);
			if(obstacleList != null && obstacleList.Length > 0)
			{
				Color old = Gizmos.color;
				Gizmos.color = ObstacleColor;
				/*
				Vector3 position;
				foreach(GameObject obstacle in obstacleList)
				{
					position = GetGridCellCenter(GetGridIndex(obstacle.transform.position));
					if(IsInBounds(position))
						Gizmos.DrawCube(position, cubicCellSize);
				}
				*/
				foreach(Node node in Nodes)
				{
					if(node.IsObstacle)
						Gizmos.DrawCube(node.Position, cubicCellSize);
				}
				Gizmos.color = old;
			}
		}
	}

	#endregion



	#region PUBLIC METHODS

	/// <summary>
	/// Gets the center of the specified grid cell.
	/// </summary>
	/// <returns>The grid cell's center.</returns>
	/// <param name="_Index">The cell's index.</param>
	public Vector3 GetGridCellCenter(int _Index)
	{
		Vector3 cellPosition = GetGridCellPosition(_Index);
		cellPosition.x += CellSize / 2.0f;
		cellPosition.z += CellSize / 2.0f;

		return cellPosition;
	}

	/// <summary>
	/// Gets the position of the specified grid cell.
	/// </summary>
	/// <returns>The grid cell's position.</returns>
	/// <param name="_Index">The cell's index.</param>
	public Vector3 GetGridCellPosition(int _Index)
	{
		int row = GetRow(_Index);
		int col = GetColumn(_Index);
		float xPositionInGrid = col * CellSize;
		float zPositionInGrid = row * CellSize;

		return Origin + new Vector3(xPositionInGrid, 0.0f, zPositionInGrid);
	}

	/// <summary>
	/// Gets the grid cell index of the cell at the specified position.
	/// </summary>
	/// <returns>The grid cell index.</returns>
	/// <param name="_Position">The position.</param>
	public int GetGridIndex(Vector3 _Position)
	{
		// Abort if the position is out of the grids bounds
		if(!IsInBounds(_Position))
			return -1;

		_Position -= Origin;
		int col = (int) (_Position.x / CellSize);
		int row = (int) (_Position.z / CellSize);

		return (row * Columns + col);
	}

	/// <summary>
	/// Determines whether the specified position is inside the grid's bounds.
	/// </summary>
	/// <returns><c>true</c> if the position is in bounds, otherwise <c>false</c>.</returns>
	/// <param name="_Position">The position.</param>
	public bool IsInBounds(Vector3 _Position)
	{
		float width = Columns * CellSize;
		float height = Rows * CellSize;
		return (_Position.x >= Origin.x && _Position.x <= Origin.x + width
		        && _Position.z >= Origin.z && _Position.z <= Origin.z + height);
	}

	/// <summary>
	/// Determines whether the specified coordinates are valid.
	/// </summary>
	/// <returns><c>true</c> if the coordinates are within the grid, otherwise <c>false</c>.</returns>
	/// <param name="_Row">The row.</param>
	/// <param name="_Column">The column.</param>
	public bool IsValid(int _Row, int _Column)
	{
		return (_Row >= 0 && _Column >= 0 && _Row < Rows && _Column < Columns);
	}

	/// <summary>
	/// Determines whether the cell at the specified coordinates is free or occupied.
	/// </summary>
	/// <returns><c>true</c> if the cell is free, otherwise <c>false</c>.</returns>
	/// <param name="_Row">The cell's row.</param>
	/// <param name="_Column">The cell's column.</param>
	public bool IsFree(int _Row, int _Column)
	{
		if(!IsValid(_Row, _Column))
			return false;

		return !Nodes[_Row, _Column].IsObstacle;
	}

	/// <summary>
	/// Determines whether the cell with the specified index is free or occupied.
	/// </summary>
	/// <returns><c>true</c> if the cell is free, otherwise <c>false</c>.</returns>
	/// <param name="_Index">The cell's index.</param>
	public bool IsFree(int _Index)
	{
		int row = GetRow(_Index);
		int col = GetColumn(_Index);

		return IsFree(row, col);
	}

	/// <summary>
	/// Gets the row of the specified grid cell index.
	/// </summary>
	/// <returns>The row.</returns>
	/// <param name="_Index">The index.</param>
	public int GetRow(int _Index)
	{
		int row = _Index / Columns;
		return row;
	}

	/// <summary>
	/// Gets the column of the specified grid cell index.
	/// </summary>
	/// <returns>The column.</returns>
	/// <param name="_Index">The index.</param>
	public int GetColumn(int _Index)
	{
		int col = _Index % Columns;
		return col;
	}

	/// <summary>
	/// Gets the neighboring nodes of the specified node.
	/// </summary>
	/// <param name="_Node">The center node.</param>
	/// <param name="_Neighbors">The list of neighboring nodes.</param>
	public void GetNeighbors(Node _Node, List<Node> _Neighbors)
	{ 
		Vector3 neighborPosition = _Node.Position;
		int neighborIndex = GetGridIndex(neighborPosition);

		int row = GetRow(neighborIndex);
		int col = GetColumn(neighborIndex);

		// Bottom
		int nodeRow = row - 1;
		int nodeColumn = col;
		AssignNeighbor(nodeRow, nodeColumn, _Neighbors);

		// Top
		nodeRow = row + 1;
		nodeColumn = col;
		AssignNeighbor(nodeRow, nodeColumn, _Neighbors);

		// Left
		nodeRow = row;
		nodeColumn = col - 1;
		AssignNeighbor(nodeRow, nodeColumn, _Neighbors);

		// Right
		nodeRow = row;
		nodeColumn = col + 1;
		AssignNeighbor(nodeRow, nodeColumn, _Neighbors);
	}

	/// <summary>
	/// Draws a debug grid.
	/// </summary>
	/// <param name="_Origin">The grid's origin.</param>
	/// <param name="_Rows">The rows.</param>
	/// <param name="_Columns">The columns.</param>
	/// <param name="_CellSize">The single cell size.</param>
	/// <param name="_Color">The color.</param>
	public void DebugDrawGrid(Vector3 _Origin, int _Rows, int _Columns, float _CellSize, Color _Color)
	{
		float width = _Columns * _CellSize;
		float height = _Rows * _CellSize;

		Vector3 startPosition;
		Vector3 endPosition;

		// Horizontal lines
		for(int i = 0; i <= _Rows; i++)
		{
			startPosition = _Origin + i * _CellSize * Vector3.forward;
			endPosition = startPosition + width * Vector3.right;
			Debug.DrawLine(startPosition, endPosition, _Color);
		}

		// Vertical lines
		for(int i = 0; i <= _Columns; i++)
		{
			startPosition = _Origin + i * _CellSize * Vector3.right;
			endPosition = startPosition + height * Vector3.forward;
			Debug.DrawLine(startPosition, endPosition, _Color);
		}
	}

	/// <summary>
	/// Occupies the cell, marking it as an obstacle.
	/// </summary>
	/// <param name="_Position">The cell's position.</param>
	public void OccupyCell(Vector3 _Position)
	{
		int index = GetGridIndex(_Position);
		//Debug.Log(this.name + " - GridManager: Occupying cell " + index + " at position " + _Position);
		Nodes[GetRow(index), GetColumn(index)].MarkAsObstacle();
	}

	/// <summary>
	/// Frees the cell, marking it as accessible.
	/// </summary>
	/// <param name="_Position">The cell's position.</param>
	public void FreeCell(Vector3 _Position)
	{
		int index = GetGridIndex(_Position);
		//Debug.Log(this.name + " - GridManager: Freeing cell " + index + " at position " + _Position);
		Nodes[GetRow(index), GetColumn(index)].MarkAsAccessible();
	}

	/// <summary>
	/// Gets a random grid cell.
	/// </summary>
	/// <returns>The random grid cell.</returns>
	/// <param name="_Center">The center.</param>
	/// <param name="_Range">The range.</param>
	public Node GetRandomGridCell(Vector3 _Center, int _Range)
	{
		int center = GetGridIndex(_Center);

		int row = Random.Range(0, _Range + 1);
		int col = _Range - row;

		if(Random.Range(0.0f, 1.0f) > 0.5f)
			row *= -1;
		if(Random.Range(0.0f, 1.0f) > 0.5f)
			col *= -1;

		row = Mathf.Clamp(row + GetRow(center), 0, Rows - 1);
		col = Mathf.Clamp(col + GetColumn(center), 0, Columns - 1);

		return Nodes[row, col];
	}

	/// <summary>
	/// Gets the closest adjacent cell at the target position from one position to the target position.
	/// </summary>
	/// <returns>The closest adjacent cell's index.</returns>
	/// <param name="_Position">The current position.</param>
	/// <param name="_Target">The target position.</param>
	public int GetClosestAdjacentCellAt(Vector3 _Position, Vector3 _Target)
	{
		int target = GetGridIndex(_Target);

		Vector3 direction = _Position - _Target;

		int row = GetRow(target);
		int col = GetColumn(target);

		for(int i = 0; i < 4; i++)
		{
			switch(i)
			{
				case 0:
				{
					if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
						col += 1 * (int)Mathf.Sign(direction.x);
					else
						row += 1 * (int)Mathf.Sign(direction.z);
				} break;

				case 1:
				{
					if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
						row += 1 * (int)Mathf.Sign(direction.z);
					else
						col += 1 * (int)Mathf.Sign(direction.x);
				} break;

				case 2:
				{
					if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
						col += -1 * (int)Mathf.Sign(direction.x);
					else
						row += -1 * (int)Mathf.Sign(direction.z);
				} break;

				case 3:
				{
					if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
						row += -1 * (int)Mathf.Sign(direction.z);
					else
						col += -1 * (int)Mathf.Sign(direction.x);
				} break;
			}

			// Return valid adjacent cell
			if(IsValid(row, col) && !Nodes[row, col].IsObstacle)
			{
				//Debug.Log("case " + i + " - target: index=" + target + ", row=" + GetRow(target) + ", col=" + GetColumn(target) + ", adjacent: index=" + (row * Columns + col) + ", row=" + row + ", col=" + col);
				break;
			}
			// No adjacent cell is valid, return own cell
			else if(i == 3)
			{
				return GetGridIndex(_Position);
			}
			// Check next adjacent cell
			else
			{
				row = GetRow(target);
				col = GetColumn(target);
			}
		}

		//Debug.Log("target: index=" + target + ", row=" + GetRow(target) + ", col=" + GetColumn(target) + ", adjacent: index=" + (row * Columns + col) + ", row=" + row + ", col=" + col);

		return row * Columns + col;
	}

	/// <summary>
	/// Gets the closest adjacent cell at the specified position facing the target position.
	/// </summary>
	/// <returns>The closest adjacent cell's index.</returns>
	/// <param name="_Position">The current position.</param>
	/// <param name="_Target">The target position.</param>
	public int GetClosestAdjacentCellTo(Vector3 _Position, Vector3 _Target)
	{
		int target = GetGridIndex(_Position);
		
		Vector3 direction = _Target - _Position;
		
		int row = GetRow(target);
		int col = GetColumn(target);
		
		for(int i = 0; i < 4; i++)
		{
			switch(i)
			{
				case 0:
					{
						if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
							col += 1 * (int)Mathf.Sign(direction.x);
						else
							row += 1 * (int)Mathf.Sign(direction.z);
					} break;
					
				case 1:
					{
						if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
							row += 1 * (int)Mathf.Sign(direction.z);
						else
							col += 1 * (int)Mathf.Sign(direction.x);
					} break;
					
				case 2:
					{
						if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
							col += -1 * (int)Mathf.Sign(direction.x);
						else
							row += -1 * (int)Mathf.Sign(direction.z);
					} break;
					
				case 3:
					{
						if(Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
							row += -1 * (int)Mathf.Sign(direction.z);
						else
							col += -1 * (int)Mathf.Sign(direction.x);
					} break;
			}

			// Return valid adjacent cell
			if(IsValid(row, col) && !Nodes[row, col].IsObstacle)
			{
				//Debug.Log("case " + i + " - target: index=" + target + ", row=" + GetRow(target) + ", col=" + GetColumn(target) + ", adjacent: index=" + (row * Columns + col) + ", row=" + row + ", col=" + col);
				break;
			}
			// No adjacent cell is valid, return own cell
			else if(i == 3)
			{
				return target;
			}
			// Check next adjacent cell
			else
			{
				row = GetRow(target);
				col = GetColumn(target);
			}
		}
		
		//Debug.Log("target: index=" + target + ", row=" + GetRow(target) + ", col=" + GetColumn(target) + ", adjacent: index=" + (row * Columns + col) + ", row=" + row + ", col=" + col);
		
		return row * Columns + col;
	}

	#endregion



	#region PRIVATE METHODS

	/// <summary>
	/// Creates a new grid considering all registered obstacles.
	/// </summary>
	private void CalculateObstacles()
	{
		// Create new empty grid
		Nodes = new Node[Rows, Columns];
		int index = 0;
		for(int r = 0; r < Rows; r++)
		{
			for(int c = 0; c < Columns; c++)
			{
				Vector3 cellPosition = GetGridCellCenter(index);
				//Debug.Log("index=" + index + ", center=" + cellPosition);
				Node node = new Node(cellPosition);
				Nodes[r, c] = node;
				index++;
			}
		}

		// Set cell occupation if there is at least one registered obstacle
		if(obstacleList != null && obstacleList.Length > 0)
		{
			foreach(GameObject obstacle in obstacleList)
			{
				for(int r = 0; r < Rows; r++)
				{
					for(int c = 0; c < Columns; c++)
					{
						if(Nodes[r, c].IsObstacle)
							continue;

						Bounds cell = new Bounds(Nodes[r, c].Position, new Vector3(CellSize, CellSize, CellSize));
						if(obstacle.renderer.bounds.Intersects(cell))
							Nodes[r, c].MarkAsObstacle();
					}
				}
			}
		}
	}

	/// <summary>
	/// Assigns a neighboring node to the node list.
	/// </summary>
	/// <param name="_Row">The neighbor's row.</param>
	/// <param name="_Column">The neighbor's column.</param>
	/// <param name="_Neighbors">The neighbor node list.</param>
	private void AssignNeighbor(int _Row, int _Column, List<Node> _Neighbors)
	{
		if(_Row != -1 && _Column != -1 && _Row < Rows && _Column < Columns)
		{
			Node node = Nodes[_Row, _Column];
			if(!node.IsObstacle)
				_Neighbors.Add(node);
		}
	}

	#endregion
}

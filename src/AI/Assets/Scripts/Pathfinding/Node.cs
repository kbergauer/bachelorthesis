﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Class representing a node in the pathfinding grid.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
[System.Serializable]
public class Node : IComparable 
{
	#region PUBLIC VARIABLES

	public float NodeTotalCost;
	public float EstimatedCost;
	public bool IsObstacle;
	public Node Parent;
	public Vector3 Position;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES
	#endregion



	#region CONSTRUCTORS

	public Node()
	{
		this.NodeTotalCost = 1.0f;
		this.EstimatedCost = 0.0f;
		this.IsObstacle = false;
		this.Parent = null;
		this.Position = Vector3.zero;
	}

	public Node(Vector3 _Position)
	{
		this.NodeTotalCost = 1.0f;
		this.EstimatedCost = 0.0f;
		this.IsObstacle = false;
		this.Parent = null;
		this.Position = _Position;
	}

	#endregion



	#region PUBLIC METHODS

	/// <summary>
	/// Marks the node as an obstacle.
	/// </summary>
	public void MarkAsObstacle()
	{
		this.IsObstacle = true;
	}

	/// <summary>
	/// Marks the node as accessible.
	/// </summary>
	public void MarkAsAccessible()
	{
		this.IsObstacle = false;
	}

	/// <summary>
	/// Compares this instance with another object of the same type returning an integer indicating,
	/// whether this instance comes before or after the object in the sorting order or if they share 
	/// the same position.
	/// </summary>
	/// <returns>
	/// A negative value, if the object comes before this instance. 
	/// A positive value, if the object comes after this instance.
	/// Zero, if they share the same position in the sorting order.
	/// </returns>
	/// <param name="_Object">The object to compare to.</param>
	public int CompareTo(object _Object)
	{
		Node node = (Node)_Object;

		if(this.EstimatedCost < node.EstimatedCost)
			return -1;

		if(this.EstimatedCost > node.EstimatedCost)
			return 1;

		return 0;
	}

	#endregion



	#region PRIVATE METHODS
	#endregion
}

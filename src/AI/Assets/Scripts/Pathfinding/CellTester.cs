﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class used for testing grid cells.
/// </summary>
public class CellTester : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public bool Output = true;
	public bool Highlight = true;

	public bool valid = false;
	public Vector3 position;
	public Vector3 size;

	public int index;
	public int row;
	public int col;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		size = new Vector3(GridManager.Instance.CellSize, GridManager.Instance.CellSize, GridManager.Instance.CellSize);
	}

	void Update()
	{
		/*
		if(Input.GetMouseButtonDown(0))
		{
			Debug.Log("Update click.");

			position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			
			index = GridManager.Instance.GetGridIndex(position);
			row = GridManager.Instance.GetRow(index);
			col = GridManager.Instance.GetColumn(index);
			if(index >= 0)
			{
				valid = true;
				position = GridManager.Instance.GetGridCellCenter(index);
			}
			
			if(Output)
			{
				if(valid)
					Debug.Log("Found valid cell with index " + index + " in row " + row + " and column " + col + " at position " + position);
				else
					Debug.Log("No valid cell at " + position);
			}
		}
		*/

		if(Input.GetKeyUp(KeyCode.P))
		{
			index = GridManager.Instance.GetGridIndex(position);
			row = GridManager.Instance.GetRow(index);
			col = GridManager.Instance.GetColumn(index);
			if(index >= 0)
			{
				valid = true;
				position = GridManager.Instance.GetGridCellCenter(index);
			}
			
			if(Output)
			{
				if(valid)
					Debug.Log("Found valid cell with index " + index + " in row " + row + " and column " + col + " at position " + position);
				else
					Debug.Log("No valid cell at " + position);
			}
		}

		if(Input.GetKeyUp(KeyCode.I))
		{
			row = GridManager.Instance.GetRow(index);
			col = GridManager.Instance.GetColumn(index);
			if(index >= 0)
			{
				valid = true;
				position = GridManager.Instance.GetGridCellCenter(index);
			}
			
			if(Output)
			{
				if(valid)
					Debug.Log("Found valid cell with index " + index + " in row " + row + " and column " + col + " at position " + position);
				else
					Debug.Log("No valid cell at " + position);
			}
		}

		if(Input.GetKeyUp(KeyCode.O))
		{
			index = row * GridManager.Instance.Columns + col;
			Debug.Log("index = " + row + " * " + GridManager.Instance.Columns + " + " + col);

			if(index >= 0)
			{
				valid = true;
				position = GridManager.Instance.Nodes[row, col].Position;
			}
			
			if(Output)
			{
				if(valid)
					Debug.Log("Found valid cell with index " + index + " in row " + row + " and column " + col + " at position " + position);
				else
					Debug.Log("No valid cell at " + position);
			}
		}
	}

	void OnDrawGizmos()
	{
		if(Highlight && valid)
		{
			Debug.DrawLine(new Vector3(position.x, 0.0f, 0.0f), position);
			Debug.DrawLine(new Vector3(0.0f, 0.0f, position.z), position);
			Gizmos.DrawCube(position, size);
		}
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

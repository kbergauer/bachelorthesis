﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class tracking a path on the A* grid.
/// </summary>
public class PathTracker : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	
	public List<Node> PathArray;
	public bool ShowPath = true;
	public Color PathColor = Color.green;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	#region PROPERTIES
	
	public Node StartNode
	{
		get;
		set;
	}
	
	public Node GoalNode
	{
		get;
		set;
	}
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	void Start()
	{
		PathArray = new List<Node>();

		StartNode = null;
		GoalNode = null;
	}
	
	void OnDrawGizmos()
	{
		if(PathArray == null || !ShowPath)
			return;

		Color old = Gizmos.color;
		
		if(PathArray.Count > 0)
		{
			int index = 0;
			Gizmos.color = PathColor;
			foreach(Node node in PathArray)
			{
				if(index < PathArray.Count)
				{
					//Node nextNode = (Node)PathArray[index];
					Gizmos.DrawSphere(node.Position, 0.25f);
					//Debug.DrawLine(node.Position, nextNode.Position, PathColor);
					index++;
				}
			}

		}

		/*
		Gizmos.color = Color.cyan;
		if(StartNode != null)
			Gizmos.DrawCube(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(StartNode.Position)),
			                new Vector3(GridManager.Instance.CellSize, GridManager.Instance.CellSize, GridManager.Instance.CellSize));
		
		Gizmos.color = Color.yellow;
		if(GoalNode != null)
			Gizmos.DrawCube(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(GoalNode.Position)),
			                new Vector3(GridManager.Instance.CellSize, GridManager.Instance.CellSize * 2.0f, GridManager.Instance.CellSize));
		*/

		Gizmos.color = old;
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Finds a path to the specified position on the grid using A*.
	/// </summary>
	/// <returns><c>true</c>, if a path was found, <c>false</c> otherwise.</returns>
	/// <param name="_Position">The goal position.</param>
	public bool FindPath(Vector3 _Position)
	{
		StartNode = new Node(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(transform.position)));

		int index = GridManager.Instance.GetGridIndex(_Position);
		if(index < 0)
		{
			//Debug.LogWarning(this.name + " - PathTracker: Goal node index out of range.");
			return false;
		}

		GoalNode = new Node(GridManager.Instance.GetGridCellCenter(index));

		if(GoalNode.IsObstacle)
		{
			//Debug.LogWarning(this.name + " - PathTracker: Goal node obstructed.");
			return false;
		}

		return FinalizePath();
	}

	/// <summary>
	/// Finds a path to the specified node on the grid using A*.
	/// </summary>
	/// <returns><c>true</c>, if a path was found, <c>false</c> otherwise.</returns>
	/// <param name="_Goal">The goal node.</param>
	public bool FindPath(Node _Goal)
	{
		StartNode = new Node(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(transform.position)));
		GoalNode = _Goal;

		if(GoalNode == null || GoalNode.IsObstacle)
			return false;
		
		return FinalizePath();
	}

	/// <summary>
	/// Returns the next node of the current path.
	/// </summary>
	/// <returns>The next node.</returns>
	public Node GetNext()
	{
		Node next = null;

		if(PathArray != null && PathArray.Count > 0)
		{
			next = (Node)PathArray[0];
			PathArray.RemoveAt(0);
		}

		return next;
	}

	/// <summary>
	/// Returns the final node of the current path.
	/// </summary>
	/// <returns>The final node.</returns>
	public Node GetLast()
	{
		Node last = null;
		
		if(PathArray != null && PathArray.Count > 0)
			last = (Node)PathArray[PathArray.Count - 1];
		
		return last;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	/// <summary>
	/// Finalizes the path by finding it and removing the starting node.
	/// </summary>
	/// <returns><c>true</c>, if a valid path was created, <c>false</c> otherwise.</returns>
	private bool FinalizePath()
	{
		PathArray = AStar.FindPath(StartNode, GoalNode);
		
		if(PathArray != null && PathArray.Count > 0)
		{
			// Remove current node from path
			PathArray.RemoveAt(0);

			return true;
		}

		return false;
	}

	#endregion
}

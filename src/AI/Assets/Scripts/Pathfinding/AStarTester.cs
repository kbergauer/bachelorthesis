﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class testing A* path finding.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class AStarTester : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public List<Node> PathArray;
	public float IntervalTime = 1.0f;
	public Color PathColor = Color.green;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES

	private Transform startPosition, endPosition;
	private GameObject StartCube, EndCube;
	private float elapsedTime = 0.0f;

	#endregion

	#region PROPERTIES

	public Node StartNode
	{
		get;
		set;
	}

	public Node GoalNode
	{
		get;
		set;
	}

	#endregion



	#region MONOBEHAVIOUR

	void Start()
	{
		StartCube = GameObject.FindGameObjectWithTag("Start");
		EndCube = GameObject.FindGameObjectWithTag("End");

		PathArray = new List<Node>();
		FindPath();
	}

	void Update()
	{
		elapsedTime += Time.deltaTime;
		if(elapsedTime >= IntervalTime)
		{
			elapsedTime = 0.0f;
			FindPath();
		}
	}

	void OnDrawGizmos()
	{
		if(PathArray == null)
			return;

		if(PathArray.Count > 0)
		{
			int index = 0;
			Color old = Gizmos.color;
			Gizmos.color = PathColor;
			foreach(Node node in PathArray)
			{
				if(index < PathArray.Count)
				{
					//Node nextNode = (Node)PathArray[index];
					Gizmos.DrawSphere(node.Position, 0.25f);
					//Debug.DrawLine(node.Position, nextNode.Position, PathColor);
					index++;
				}
			}
			Gizmos.color = old;
		}
	}

	#endregion



	#region PUBLIC METHODS
	#endregion



	#region PRIVATE METHODS

	private void FindPath()
	{
		startPosition = StartCube.transform;
		endPosition = EndCube.transform;

		StartNode = new Node(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(startPosition.position)));
		GoalNode = new Node(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(endPosition.position)));

		PathArray = AStar.FindPath(StartNode, GoalNode);
	}

	#endregion
}

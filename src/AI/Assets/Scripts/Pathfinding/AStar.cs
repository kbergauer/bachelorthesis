﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class handling A* pathfinding.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class AStar 
{
	#region PUBLIC VARIABLES

	public static PriorityQueue ClosedList, OpenList;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES
	#endregion


	
	#region PUBLIC METHODS

	/// <summary>
	/// Finds the shortest path from the starting to the goal node.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="_Start">The starting node.</param>
	/// <param name="_Goal">The goal node.</param>
	public static List<Node> FindPath(Node _Start, Node _Goal)
	{
		// Initializes lists
		OpenList = new PriorityQueue();
		OpenList.Push(_Start);
		_Start.NodeTotalCost = 0.0f;
		_Start.EstimatedCost = HeurisiticEstimateCost(_Start, _Goal);

		ClosedList = new PriorityQueue();
		Node node = null;

		//float time = Time.time;

		// Start A*
		while(OpenList.Length != 0)
		{
			// Get next node
			node = OpenList.First();
			// Abort if goal node was reached
			if(node.Position == _Goal.Position)
				return CalculatePath(node);

			// Get neighboring nodes
			List<Node> neighbors = new List<Node>();
			GridManager.Instance.GetNeighbors(node, neighbors);

			for(int i = 0; i < neighbors.Count; i++)
			{
				Node neighborNode = (Node)neighbors[i];

				if(!ClosedList.Contains(neighborNode))
				{
					float cost = HeurisiticEstimateCost(node, neighborNode);
					float totalCost = node.NodeTotalCost + cost;
					float neighborNodeEstCost = HeurisiticEstimateCost(neighborNode, _Goal);

					neighborNode.NodeTotalCost = totalCost;
					neighborNode.Parent = node;
					neighborNode.EstimatedCost = totalCost + neighborNodeEstCost;

					if(!OpenList.Contains(neighborNode))
						OpenList.Push(neighborNode);
				}
			}

			ClosedList.Push(node);
			OpenList.Remove(node);
		}
			
		//time = Time.time - time;
		//Debug.Log("A*: FindPath(...) completed after " + time + " seconds.");

		if(node.Position != _Goal.Position)
		{
			//Debug.LogWarning("A*: Unable to reach goal.");
			return null;
		}

		return CalculatePath(node);
	}

	#endregion



	#region PRIVATE METHODS

	/// <summary>
	/// Calculates the estimated cost from the current to the goal node.
	/// </summary>
	/// <returns>The estimated cost.</returns>
	/// <param name="_Current">The current node.</param>
	/// <param name="_Goal">The goal node.</param>
	private static float HeurisiticEstimateCost(Node _Current, Node _Goal)
	{
		Vector3 cost = _Current.Position - _Goal.Position;
		return cost.magnitude;
	}

	/// <summary>
	/// Backtracks the path from the specified node to the starting parent.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="_Node">The node to start backtracking from.</param>
	private static List<Node> CalculatePath(Node _Node)
	{
		List<Node> path = new List<Node>();
		while(_Node != null)
		{
			path.Add(_Node);
			_Node = _Node.Parent;
		}

		path.Reverse();
		return path;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a priority queue for pathfinding.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class PriorityQueue 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES

	private List<Node> nodes = new List<Node>();

	#endregion
	
	#region PROPERTIES

	/// <summary>
	/// Gets the length of the node list.
	/// </summary>
	/// <value>The length.</value>
	public int Length
	{
		get { return this.nodes.Count;	}
	}

	#endregion



	#region CONSTRUCTORS
	#endregion



	#region PUBLIC METHODS

	/// <summary>
	/// Checks, whether the specified node is contained within the node list.
	/// </summary>
	/// <returns><c>true</c> if the node is contained, otherwise <c>false</c></returns>
	/// <param name="_Node">The node.</param>
	public bool Contains(Node _Node)
	{
		return this.nodes.Contains(_Node);
	}

	/// <summary>
	/// Returns the first node of the list.
	/// </summary>
	/// <returns>The first node if possible, otherwise <c>null</c></returns>
	public Node First()
	{
		if(this.nodes.Count > 0)
			return (Node)this.nodes[0];

		return null;
	}

	/// <summary>
	/// Adds the specified node to the list and sorts it afterwards.
	/// </summary>
	/// <param name="_Node">The node to be added.</param>
	public void Push(Node _Node)
	{
		this.nodes.Add(_Node);
		this.nodes.Sort();
	}

	/// <summary>
	/// Remove the specified node from the list and sorts if afterwards.
	/// </summary>
	/// <param name="_Node">The node to be removed.</param>
	public void Remove(Node _Node)
	{
		this.nodes.Remove(_Node);
		this.nodes.Sort();
	}

	#endregion



	#region PRIVATE METHODS
	#endregion
}

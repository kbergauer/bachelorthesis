﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a default item.
/// </summary>
[System.Serializable]
public class Item 
{
	#region PUBLIC VARIABLES

	public enum EType
	{
		Junk,
		Weapon,
		Armor
	}

	[Tooltip("The item's name.")]
	public string Name = "Unidentified Item";
	[Tooltip("The item's type.")]
	public EType Type = EType.Junk;
	[Tooltip("The amount of space the item occupies.")]
	public float Volume = 1.0f;
	[Tooltip("The value of the item in gold.")]
	public int Value = 0;
	[Tooltip("The item's added attack value.")]
	public float Attack = 0.0f;
	[Tooltip("The item's added defense value.")]
	public float Defense = 0.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes a new instance of the <see cref="Item"/> class with default values.
	/// </summary>
	public Item()
	{
		Name = "Unidentified Item";
		Type = EType.Junk;
		Volume = 1.0f;
		Value = 0;
		Attack = 0.0f;
		Defense = 0.0f;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Item"/> class.
	/// </summary>
	/// <param name="_Name">The item's name.</param>
	/// <param name="_Volume">The item's volume.</param>
	public Item(string _Name, float _Volume)
	{
		Name = _Name;
		Type = EType.Junk;
		Volume = _Volume;
		Value = 0;
		Attack = 0.0f;
		Defense = 0.0f;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Item"/> class.
	/// </summary>
	/// <param name="_Name">The item's name.</param>
	/// <param name="_Type">The item's type.</param>
	/// <param name="_Volume">The item's volume.</param>
	/// <param name="_Value">The item's value in gold.</param>
	public Item(string _Name, EType _Type, float _Volume, int _Value)
	{
		Name = _Name;
		Type = _Type;
		Volume = _Volume;
		Value = _Value;
		Attack = 0.0f;
		Defense = 0.0f;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Item"/> class.
	/// </summary>
	/// <param name="_Name">The item's name.</param>
	/// <param name="_Type">The item's type.</param>
	/// <param name="_Volume">The item's volume.</param>
	/// <param name="_Value">The item's value in gold.</param>
	/// <param name="_Attack">The item's attack value.</param>
	/// <param name="_Defense">The item's defense value.</param>
	public Item(string _Name, EType _Type, float _Volume, int _Value, float _Attack, float _Defense)
	{
		Name = _Name;
		Type = _Type;
		Volume = _Volume;
		Value = _Value;
		Attack = _Attack;
		Defense = _Defense;
	}

	/// <summary>
	/// Creates a completely random item.
	/// </summary>
	/// <returns>The random item.</returns>
	static public Item GetRandom()
	{
		return GetRandom((EType)Random.Range(0, System.Enum.GetValues(typeof(EType)).Length));
	}

	/// <summary>
	/// Creates a random item of the specified type.
	/// </summary>
	/// <returns>The random item.</returns>
	/// <param name="_Type">The type of item to create.</param>
	static public Item GetRandom(EType _Type)
	{
		Item item = new Item();
		item.Type = _Type;
		item.Name = item.Type.ToString() + " #" + Random.Range(0, 1000);

		switch(item.Type)
		{
			case EType.Weapon:
			{
				item.Volume = Random.Range(50, 251) / 100;
				item.Value = Random.Range(400, 2001);
				item.Attack = Random.Range(5, 11);
				item.Defense = Random.Range(0, 1);
			} break;

			case EType.Armor:
			{
				item.Volume = Random.Range(200, 501) / 100;
				item.Value = Random.Range(500, 2501);
				item.Attack = Random.Range(0, 1);
				item.Defense = Random.Range(5, 11);
			} break;

			case EType.Junk:
			{
				item.Volume = Random.Range(1, 150) / 100;
				item.Value = Random.Range(0, 1001);
			} break;
		}

		return item;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

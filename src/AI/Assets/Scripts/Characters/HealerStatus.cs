using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a healing character in the game.
/// </summary>
public class HealerStatus : CharacterStatus 
{
	#region PUBLIC VARIABLES

	public float Healing = 20.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private List<CharacterStatus> targets = new List<CharacterStatus>();

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Heal the specified target by a set amount specified in <c>Healing</c>.
	/// </summary>
	/// <param name="_Target">The target.</param>
	public void Heal(CharacterStatus _Target)
	{
		_Target.DirectHealthAdjust(Healing);
	}

	/// <summary>
	/// Requests adds a target to the target list.
	/// </summary>
	/// <param name="_Target">The target requesting healing.</param>
	public void RequestHeal(CharacterStatus _Target)
	{
		if(_Target != null && targets != null)
			targets.Add(_Target);
	}

	/// <summary>
	/// Gets the next target from the target list.
	/// </summary>
	/// <returns>The next target.</returns>
	public CharacterStatus GetNextTarget()
	{
		CharacterStatus next = null;

		if(targets != null && targets.Count > 0)
		{
			next = targets[0];
			targets.RemoveAt(0);
		}

		return next;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

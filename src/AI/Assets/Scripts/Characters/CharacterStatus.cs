﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a default role-playing character in the game.
/// </summary>
public class CharacterStatus : MonoBehaviour 
{
	#region EVENTS

	public delegate void HealthAdjustHandler(float _Sign);
	public event HealthAdjustHandler HealthAdjustment;

	#endregion

	#region PUBLIC VARIABLES

	[Tooltip("The character's maximum health.")]
	public float MaximumHealth = 100.0f;
	[Tooltip("The character's current health.")]
	public float CurrentHealth = 100.0f;
	[Tooltip("The character's attack value.")]
	public float Attack = 10.0f;
	[Tooltip("The character's defense value.")]
	public float Defense = 10.0f;

	[Tooltip("The character's current level.")]
	public int Level = 1;
	[Tooltip("The character's maximum level.")]
	public int MaximumLevel = 10;
	[Tooltip("The character's current experience.")]
	public int Experience = 0;
	[Tooltip("The character's necessary experience to advance to the next level.")]
	public int NecessaryExperience = 10;
	[Tooltip("The character's experience increase factor each level.")]
	public float ExperienceIncrease = 2.0f;
	[Tooltip("Experience awarded to the killer of this character.")]
	public int ExperienceReward = 1;
	[Tooltip("Chance to drop an item on death.")]
	public float DropChance = 1.0f;

	[Tooltip("The character's current power level.")]
	public float PowerLevel = 0.0f;
	
	public Inventory Inventory;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		// Initialize inventory
		Inventory.Initialize();

		// Get initial power level
		CalculatePowerLevel();
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Gets the character's total attack value.
	/// </summary>
	/// <returns>The total attack.</returns>
	public float GetTotalAttack()
	{
		return Attack + (Inventory.Weapon != null ? Inventory.Weapon.Attack : 0.0f) + (Inventory.Armor != null ? Inventory.Armor.Attack : 0.0f);
	}

	/// <summary>
	/// Gets the character's total defense value.
	/// </summary>
	/// <returns>The total defense.</returns>
	public float GetTotalDefense()
	{
		return Defense + (Inventory.Weapon != null ? Inventory.Weapon.Defense : 0.0f) + (Inventory.Armor != null ? Inventory.Armor.Defense : 0.0f);
	}

	/// <summary>
	/// Calculates and assigns damage to the character.
	/// </summary>
	/// <param name="_Attacker">The attacker.</param>
	public bool TakeDamage(CharacterStatus _Attacker)
	{
		// Get total negative attack value
		float attack = 0.0f;
		if(_Attacker != null)
		{
			attack = -_Attacker.Attack;
			if(_Attacker.Inventory.Weapon != null)
				attack -= _Attacker.Inventory.Weapon.Attack;
			if(_Attacker.Inventory.Armor != null)
				attack -= _Attacker.Inventory.Armor.Attack;
		}

		// Add defense values
		attack += Defense;
		if(Inventory.Weapon != null)
			attack += Inventory.Weapon.Defense;
		if(Inventory.Armor != null)
			attack += Inventory.Armor.Defense;
		attack = Mathf.Min(attack, 0.0f);

		// Adjust character's health
		if(AdjustHealth(attack))
		{
			CharacterUI.Log(this.name + " was killed by " + (_Attacker != null ? _Attacker.name : " an unknown source."));

			// Drop item and reward experience to killer
			if(_Attacker != null)
			{
				_Attacker.AddExperience(ExperienceReward);
				Item drop = DropItem();
				if(drop != null)
				{
					int result = _Attacker.PickUp(drop);
					if(result == -1)
						CharacterUI.LogWarning(_Attacker.name + " is trying to store " + drop.Name + " but is already storing it; destroying it.");
					else if(result == -2)
						CharacterUI.LogWarning(_Attacker.name + " is trying to store " + drop.Name + " but is unable to store anymore items; destroying it.");
				}
			}

			// Destroy killed character
			Destroy(gameObject);
			return true;
		}

		return false;
	}

	/// <summary>
	/// Adjust the character's health directly by the specified amount.
	/// </summary>
	/// <param name="_Amount">The amount.</param>
	public void DirectHealthAdjust(float _Amount)
	{
		// Adjust character's health
		if(AdjustHealth(_Amount))
		{
			CharacterUI.Log(this.name + " died from a direct health adjustment.");

			// Destroy killed character
			Destroy(gameObject);
		}
	}

	/// <summary>
	/// Adds the specified amount to the character's experience.
	/// </summary>
	/// <param name="_Amount">The amount to add.</param>
	public void AddExperience(int _Amount)
	{
		Experience += _Amount;

		// Check necessary experience and level up, if limit reached
		if(Experience >= NecessaryExperience)
		{
			// Save excess experience to avoid experience loss
			int excess = Mathf.Min(Experience - NecessaryExperience, 0);
			if(LevelUp())
				Experience = excess;
			else
				Experience = NecessaryExperience;
		}
	}

	/// <summary>
	/// Requests a trade from the specified character.
	/// </summary>
	/// <returns>The trade component.</returns>
	/// <param name="_Other">The character requesting a trade.</param>
	public TradeOnDemand RequestTrade(CharacterStatus _Other)
	{
		CharacterUI.Log(this.name + " recieved a trade request from " + _Other.name);

		TradeOnDemand trade = null;

		if(CanTrade())
			trade = GetComponent<TradeOnDemand>();

		return trade;
	}

	/// <summary>
	/// Determines whether this character can trade.
	/// </summary>
	/// <returns><c>true</c> if this character can trade, otherwise, <c>false</c>.</returns>
	public bool CanTrade()
	{
		if(Inventory.Gold > 0 || Inventory.Bag.Count > 0)
			return true;

		return false;
	}

	/// <summary>
	/// Compares the specified item with the character's equipment.
	/// </summary>
	/// <returns><c>true</c> if equipment is worse, <c>false</c> otherwise.</returns>
	/// <param name="_Item">The item to compare to.</param>
	public bool CheckEquipment(Item _Item)
	{
		if(_Item.Type == Item.EType.Weapon)
		{
			if(Inventory.Weapon == null || _Item.Attack > Inventory.Weapon.Attack || (_Item.Attack == Inventory.Weapon.Attack && _Item.Defense > Inventory.Weapon.Defense))
				return true;
		}
		else if(_Item.Type == Item.EType.Armor)
		{
			if(Inventory.Armor == null || _Item.Defense > Inventory.Armor.Defense || (_Item.Defense == Inventory.Armor.Defense && _Item.Attack > Inventory.Armor.Attack))
				return true;
		}
		
		return false;
	}

	/// <summary>
	/// Attemps to pick the specified item up.
	/// </summary>
	/// <returns>Zero on success, otherwise error code below zero.</returns>
	/// <param name="_Item">The item to pick up.</param>
	public int PickUp(Item _Item)
	{
		// Try equipping the item, if possible
		if(_Item.Type != Item.EType.Junk && CheckEquipment(_Item))
		{
			if(Inventory.EquipWeapon(_Item) || Inventory.EquipArmor(_Item))
			{
				CalculatePowerLevel();
				return 0;
			}
		}

		// Try storing the item in the bag
		return Inventory.Store(_Item);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	/// <summary>
	/// Adjusts the character's health.
	/// </summary>
	/// <returns><c>true</c>, if health adjustment was lethal, <c>false</c> otherwise.</returns>
	/// <param name="_Adjustment">The adjustment to make, positive or negative.</param>
	private bool AdjustHealth(float _Amount)
	{
		CurrentHealth = Mathf.Clamp(CurrentHealth + _Amount, 0.0f, MaximumHealth);

		// Send health adjustment event, if it has listeners
		if(HealthAdjustment != null)
			HealthAdjustment(Mathf.Sign(_Amount));

		if(CurrentHealth == 0)
			return true;

		return false;
	}

	/// <summary>
	/// Levels the character up by one level.
	/// </summary>
	private bool LevelUp()
	{
		// Abort, if maximum level has been reached
		if(Level >= MaximumLevel)
			return false;

		Level++;
		Experience = 0;
		NecessaryExperience = (int) (NecessaryExperience * ExperienceIncrease);

		Attack++;
		Defense++;

		CalculatePowerLevel();

		return true;
	}

	/// <summary>
	/// Creates an item with a certain chance.
	/// </summary>
	/// <returns>The created item.</returns>
	private Item DropItem()
	{
		if(Random.Range(0.0f, 1.0f) > DropChance)
			return null;

		return Item.GetRandom();
	}

	/// <summary>
	/// Recalculates the character's power level.
	/// </summary>
	private void CalculatePowerLevel()
	{
		PowerLevel = Level + Attack + Defense;

		if(Inventory.Weapon != null)
		{
			PowerLevel += Inventory.Weapon.Attack;
			PowerLevel += Inventory.Weapon.Defense;
		}

		if(Inventory.Armor != null)
		{
			PowerLevel += Inventory.Armor.Attack;
			PowerLevel += Inventory.Armor.Defense;
		}
	}

	#endregion
}

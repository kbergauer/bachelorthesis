﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a character's inventory.
/// </summary>
[System.Serializable]
public class Inventory 
{
	#region PUBLIC VARIABLES

	public int Gold = 0;

	public float MaximumVolume = 10.0f;
	public float CurrentVolume = 0.0f;

	public Item Weapon = null;
	public Item Armor = null;
	public List<Item> Bag = new List<Item>();

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initialize the inventory.
	/// </summary>
	public void Initialize()
	{
		if(Weapon.Type == Item.EType.Junk)
			Weapon = null;
		if(Armor.Type == Item.EType.Junk)
			Armor = null;
	}

	/// <summary>
	/// Stores the specified item in the bag.
	/// </summary>
	/// <param name="_Item">The item to store.</param>
	public int Store(Item _Item)
	{
		if(_Item == null)
			return -3;

		if(Bag.Contains(_Item))
			return -1;
		else if(CurrentVolume + _Item.Volume > MaximumVolume)
			return -2;

		Bag.Add(_Item);
		CurrentVolume += _Item.Volume;

		return 0;
	}

	/// <summary>
	/// Removes the specified item from the bag.
	/// </summary>
	/// <param name="_Item">The item to remove.</param>
	public int Remove(Item _Item)
	{
		if(_Item == null)
			return -3;

		if(!Bag.Contains(_Item))
			return -1;

		Bag.Remove(_Item);
		CurrentVolume -= _Item.Volume;

		return 0;
	}

	/// <summary>
	/// Equips the specified item as weapon.
	/// </summary>
	/// <returns><c>true</c>, if weapon was equipped, <c>false</c> otherwise.</returns>
	/// <param name="_Weapon">The weapon.</param>
	public bool EquipWeapon(Item _Weapon)
	{
		if(_Weapon.Type != Item.EType.Weapon)
			return false;

		// Unequip and store old item
		Store(Weapon);

		// Equip new item
		Weapon = _Weapon;

		return true;
	}

	/// <summary>
	/// Equips the specified item as armor.
	/// </summary>
	/// <returns><c>true</c>, if armor was equipped, <c>false</c> otherwise.</returns>
	/// <param name="_Armor">_ armor.</param>
	public bool EquipArmor(Item _Armor)
	{
		if(_Armor.Type != Item.EType.Armor)
			return false;

		// Unequip and store old item
		Store(Armor);

		// Equip new item
		Armor = _Armor;

		return true;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

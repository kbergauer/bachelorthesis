﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A simple debug user-interface providing useful information.
/// </summary>
[ExecuteInEditMode]
public class CharacterUI : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public float BoxWidth = 100.0f;
	public float BoxHeight = 100.0f;
	public float ButtonWidth = 75.0f;
	public float ButtonHeight = 25.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private enum ETabs
	{
		Info,
		Memory,
		Affinity
	}

	private ETabs currentTab = ETabs.Info;

	private struct SMessage
	{
		public string ContentText;
		public Color ContentColor;
	}
	
	private Vector2 inventoryScrollPosition = Vector2.zero;
	private Vector2 infoScrollPosition = Vector2.zero;
	private Vector2 memoryScrollPosition = Vector2.zero;
	private Vector2 affinityScrollPosition = Vector2.zero;
	private List<SMessage> messageList = new List<SMessage>();
	private Agent agent = null;
	private CharacterStatus character = null;
	private bool showStatus = false;
	private bool showInventory = false;

	private int newMessages = 0;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		agent = GameObject.FindGameObjectWithTag("Player").GetComponent<Agent>();
		character = agent.GetComponent<CharacterStatus>();
	}

	void Update()
	{
		if(Input.GetMouseButtonUp(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit))
			{
				Agent newAgent = hit.transform.GetComponent<Agent>();
				if(newAgent != null)
				{
					agent = newAgent;
					character = agent.GetComponent<CharacterStatus>();
				}
			}
		}
	}

	void OnGUI()
	{
		// Background box
		GUI.Box(new Rect(0.0f, Screen.height - BoxHeight, BoxWidth, BoxHeight), "");
		GUILayout.BeginArea(new Rect(0.0f, Screen.height - BoxHeight, BoxWidth, BoxHeight));
		GUILayout.Space(5.0f);

		GUILayout.BeginHorizontal();

		// Control button area
		GUILayout.BeginVertical(GUILayout.Width(ButtonWidth + 10.0f));

		// Time control button
		GUILayout.BeginHorizontal();
		GUILayout.Space(5.0f);
		if(Time.timeScale > 0.0f)
		{
			if(GUILayout.Button("Pause", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
				Time.timeScale = 0.0f;
		}
		else
		{
			if(GUILayout.Button("Resume", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
				Time.timeScale = 1.0f;
		}
		GUILayout.EndHorizontal();

		// Status button
		GUILayout.BeginHorizontal();
		GUILayout.Space(5.0f);
		if(agent != null && character != null)
		{
			if(showStatus)
			{
				if(GUILayout.Button("Hide Status", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
					showStatus = false;
			}
			else
			{
				if(GUILayout.Button("Show Status", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
					showStatus = true;
			}
		}
		else
			GUILayout.Button("Show Status", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight));
		GUILayout.EndHorizontal();

		// Inventory button
		GUILayout.BeginHorizontal();
		GUILayout.Space(5.0f);
		if(character != null)
		{
			if(showInventory)
			{
				if(GUILayout.Button("Hide Inventory", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
					showInventory = false;
			}
			else
			{
				if(GUILayout.Button("Show Inventory", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight)))
					showInventory = true;
			}
		}
		else
			GUILayout.Button("Show Inventory", GUILayout.Width(ButtonWidth), GUILayout.Height(ButtonHeight));
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();


		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

		Color color = GUI.color;

		if(currentTab != ETabs.Info)
			GUI.color = Color.gray;
		else
			GUI.color = Color.white;
		if(GUILayout.Button("Info" + (currentTab != ETabs.Info && newMessages > 0 ? " (" + newMessages + ")" : ""), GUILayout.Width(ButtonWidth), GUILayout.Height(25)))
		{
			newMessages = 0;
			currentTab = ETabs.Info;
		}

		if(currentTab != ETabs.Memory)
			GUI.color = Color.gray;
		else
			GUI.color = Color.white;
		if(GUILayout.Button("Memory", GUILayout.Width(ButtonWidth), GUILayout.Height(25)))
			currentTab = ETabs.Memory;

		if(currentTab != ETabs.Affinity)
			GUI.color = Color.gray;
		else
			GUI.color = Color.white;
		if(GUILayout.Button("Affinity", GUILayout.Width(ButtonWidth), GUILayout.Height(25)))
			currentTab = ETabs.Affinity;

		GUI.color = color;

		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		switch(currentTab)
		{
		case ETabs.Info:
		{
			ShowInfo();
		} break;

		case ETabs.Memory:
		{
			ShowMemory();
		} break;

		case ETabs.Affinity:
		{
			ShowAffinity();
		} break;
		}

		GUILayout.EndVertical();


		GUILayout.EndHorizontal();


		GUILayout.Space(5.0f);
		GUILayout.EndArea();

		if(showStatus)
			ShowStatus();

		if(showInventory)
			ShowInventory();
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public static void Log(string _Message)
	{
		CharacterUI.Log(_Message, Color.white);
	}

	public static void LogWarning(string _Message)
	{
		CharacterUI.Log(_Message, Color.yellow);
	}

	public static void LogError(string _Message)
	{
		CharacterUI.Log(_Message, Color.red);
	}

	public static void Log(string _Message, Color _Color)
	{
		GameObject go = GameObject.FindGameObjectWithTag("MainCamera");
		CharacterUI ui = go.GetComponent<CharacterUI>();

		ui.AddMessage(_Message, _Color);
	}

	public void AddMessage(string _Message, Color _Color)
	{
		System.DateTime time = System.DateTime.Now;

		SMessage msg = new SMessage();
		msg.ContentText = "[" + time.Hour.ToString("00") + ":" + time.Minute.ToString("00") + ":" + time.Second.ToString("00") + "] " + _Message;
		msg.ContentColor = _Color;
		
		messageList.Insert(0, msg);

		if(currentTab != ETabs.Info)
			newMessages++;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	private void ShowStatus()
	{
		if(character == null || agent == null)
		{
			GUI.Box(new Rect(0.0f, 0.0f, 150.0f, 50.0f), "Dead - Status");
			GUILayout.BeginArea(new Rect(0.0f, 0.0f, 150.0f, 50.0f));
			GUILayout.Space(30.0f);

			GUILayout.Label(" Target is dead");

			GUILayout.EndArea();

			return;
		}

		GUI.Box(new Rect(0.0f, 0.0f, 150.0f, 200.0f), agent.name + " - Status");
		GUILayout.BeginArea(new Rect(0.0f, 0.0f, 150.0f, 200.0f));
		GUILayout.Space(30.0f);
		
		GUILayout.Label(" Health: " + character.CurrentHealth + "/" + character.MaximumHealth);
		GUILayout.Label(" Attack: " + character.GetTotalAttack() + " (" + character.Attack + " + " + ((character.Inventory.Weapon != null ? character.Inventory.Weapon.Attack : 0) + (character.Inventory.Armor != null ? character.Inventory.Armor.Attack : 0)) + ")");
		GUILayout.Label(" Defense: " + character.GetTotalDefense() + " (" + character.Defense + " + " + ((character.Inventory.Weapon != null ? character.Inventory.Weapon.Defense : 0) + (character.Inventory.Armor != null ? character.Inventory.Armor.Defense : 0)) + ")");
		GUILayout.Label(" Level: " + character.Level + "/" + character.MaximumLevel);
		GUILayout.Label(" Experience: " + character.Experience + "/" + character.NecessaryExperience);
		GUILayout.Label(" PowerLevel: " + character.PowerLevel);
		GUILayout.Label(" Action: " + (agent.CurrentAction != null ? agent.CurrentAction.Name : "None"));
		
		GUILayout.EndArea();
	}

	private void ShowInventory()
	{
		if(character == null)
		{
			GUI.Box(new Rect((showStatus ? 155.0f : 0.0f), 0.0f, 200.0f, 50.0f), "Dead - Inventory");
			GUILayout.BeginArea(new Rect((showStatus ? 155.0f : 0.0f), 0.0f, 200.0f, 50.0f));
			GUILayout.Space(30.0f);
			
			GUILayout.Label(" Target is dead");
			
			GUILayout.EndArea();
			
			return;
		}

		GUI.Box(new Rect((showStatus ? 155.0f : 0.0f), 0.0f, 200.0f, 300.0f), agent.name + " - Inventory");
		GUILayout.BeginArea(new Rect((showStatus ? 155.0f : 0.0f), 0.0f, 200.0f, 300.0f));
		GUILayout.Space(30.0f);

		GUILayout.Label(" Gold: " + character.Inventory.Gold);
		GUILayout.Label(" Volume: " + character.Inventory.CurrentVolume + "/" + character.Inventory.MaximumVolume);
		if(character.Inventory.Weapon != null)
			GUILayout.Label(" Weapon: " + character.Inventory.Weapon.Name + " (A" + character.Inventory.Weapon.Attack + "/D" + character.Inventory.Weapon.Defense + ")");
		else
			GUILayout.Label(" Weapon: none");
		if(character.Inventory.Armor != null)
			GUILayout.Label(" Armor: " + character.Inventory.Armor.Name + " (A" + character.Inventory.Armor.Attack + "/D" + character.Inventory.Armor.Defense + ")");
		else
			GUILayout.Label(" Armor: none");
		GUILayout.Label(" Bag contents:");

		inventoryScrollPosition = GUILayout.BeginScrollView(inventoryScrollPosition, false, false);
		for(int i = 0; i < character.Inventory.Bag.Count; i++)
			GUILayout.Label("   " + character.Inventory.Bag[i].Name + " (V" + character.Inventory.Bag[i].Value + "/A" + character.Inventory.Bag[i].Attack + "/D" + character.Inventory.Bag[i].Defense + ")");
		GUILayout.EndScrollView();
		
		GUILayout.EndArea();
	}

	private void ShowInfo()
	{
		infoScrollPosition = GUILayout.BeginScrollView(infoScrollPosition, false, true);
		Color color = GUI.contentColor;
		bool wrap = GUI.skin.label.wordWrap;
		GUI.skin.label.wordWrap = true;
		for(int i = 0; i < messageList.Count; i++)
		{
			GUI.contentColor = messageList[i].ContentColor;
			GUILayout.Label(messageList[i].ContentText);
		}
		GUI.skin.label.wordWrap = wrap;
		GUI.contentColor = color;
		GUILayout.EndScrollView();
	}

	private void ShowMemory()
	{
		/*
		GUI.Box(new Rect(BoxWidth + 5.0f, Screen.height - BoxHeight, Screen.width - (BoxWidth + 5.0f), BoxHeight), agent.name + " - Memory");
		GUILayout.BeginArea(new Rect(BoxWidth + 5.0f, Screen.height - BoxHeight, Screen.width - (BoxWidth + 5.0f), BoxHeight));
		GUILayout.Space(30.0f);
		*/

		string label;
		int count = 1;

		memoryScrollPosition = GUILayout.BeginScrollView(memoryScrollPosition, false, true);
		
		foreach(DictionaryEntry entry in agent.Sensors.Memory)
		{
			label = entry.Key + ": ";
			
			switch((string)entry.Key)
			{
			default:
			{
				label += entry.Value.ToString();
			} break;
				
			case "Enemy":
			case "Healer":
			case "Trader":
			case "ValidTrader":
			case "InvalidTrader":
			{
				List<Detection> list = (List<Detection>)entry.Value;
				label += "Count: " + list.Count + "; ";
				for(int i = 0; i < list.Count; i++)
					label += "[N=" + (list[i].Reference != null ? list[i].Reference.name : "None") + "/A=" + list[i].Age + "/L=" + list[i].Location + "] ";
			} break;
			}
			
			GUILayout.Label(label);
			count++;
		}

		GUILayout.EndScrollView();

		//GUILayout.EndArea();
	}

	private void ShowAffinity()
	{
		affinityScrollPosition = GUILayout.BeginScrollView(affinityScrollPosition, false, true);

		List<Action> all = new List<Action>(agent.Actions);
		all.Sort();
		List<Action> possible = agent.GetPossibleActions();
		possible.Sort();

		Color color = GUI.contentColor;

		GUI.contentColor = Color.white;
		for(int i = 0; i < possible.Count; i++)
			GUILayout.Label(possible[i].Name + ": " + possible[i].CurrentAffinity + " (Raw: " + possible[i].RawAffinity + ")");

		GUI.contentColor = Color.gray;
		for(int i = 0; i < all.Count; i++)
			if(!possible.Contains(all[i]))
				GUILayout.Label(all[i].Name + ": " + all[i].CurrentAffinity + " (Raw: " + all[i].RawAffinity + ")");

		GUI.contentColor = color;

		GUILayout.EndScrollView();
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a trade request action.
/// </summary>
public class RequestTrade : FollowPath 
{
	#region EVENTS

	public delegate void TradeHandler();
	public event TradeHandler OnAbort;
	public delegate void OfferHandler(ETradeType _Type, Item _Item);
	public event OfferHandler OnOffer;

	#endregion

	#region PUBLIC VARIABLES

	public enum ETradeType
	{
		Buy,
		Sell
	};

	public bool ShowLogs = false;
	public string TargetTag = "Trader";
	public string PositiveTagPrefix = "Valid";
	public string NegativeTagPrefix = "Invalid";
	public float RequestRange = 1.0f;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private CharacterStatus self;
	private CharacterStatus trader;
	private TradeOnDemand trade;
	private bool inRange = false;
	private bool requestSent = false;
	private bool offerOngoing = false;

	private List<Item> itemsToSell;
	private List<Item> itemsToBuy;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	new protected void Start()
	{
		base.Start();
		
		// Get own character status
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Action::RequestTrade: Unable to locate character status on game object, attack damage will be zero.");
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();
		
		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::RequestTrade: Agent is lacking sensory system, no traders in sight.");
			return;
		}

		GetTrader();
	}
	
	override public void Execute()
	{
		if(trader == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestTrade: No valid trader, looking for new one.");
			if(!GetTrader())
				return;
		}
		
		if(inRange)
		{
			// Check if trader is still in range
			if(Vector3.Distance(transform.position, trader.transform.position) >= RequestRange)
			{
				if(ShowLogs)
					Debug.Log(this.name + " - Action::RequestTrade: Trader got out of range.");
				inRange = false;
			}
		}
		
		// Move to trader
		if(!inRange)
			base.Execute();
		else
		{
			// Rotate towards trader
			Vector3 direction = trader.transform.position - transform.position;
			direction.y = 0.0f;
			direction.Normalize();
			transform.rotation = Quaternion.LookRotation(direction);
			
			// Send request
			if(!requestSent)
				SendRequest();
			else if(!offerOngoing)
				Trade();
		}
	}
	
	override public void CleanUp()
	{
		base.CleanUp();

		if(OnAbort != null)
		{
			CharacterUI.Log(this.name + " is aborting the trade with " + trader.name + ".");
			OnAbort();
		}
		
		trader = null;
		trade = null;
		requestSent = false;
		inRange = false;
		offerOngoing = false;
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellAt(transform.position, trader.transform.position)));
	}
	
	override protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
			GridManager.Instance.OccupyCell(currentNode.Position);
		}
		
		if(Vector3.Distance(transform.position, trader.transform.position) < RequestRange)
		{
			inRange = true;
		}
		else
		{
			if(!ValidatePath())
				GetNewPath();
			
			targetNode = tracker.GetNext();
			if(targetNode != null)
			{
				GridManager.Instance.OccupyCell(targetNode.Position);
				targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
			}
		}
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	
	private bool GetTrader()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestTrade: Getting new trader.");

		List<Transform> traders = agent.Sensors.GetTransformList(PositiveTagPrefix + TargetTag);
		
		if(traders == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestTrade: No traders in sensory system.");
			return false;
		}
		
		// Get closest trader
		float distance = float.PositiveInfinity;
		for(int i = 0; i < traders.Count; i++)
		{
			if(Vector3.Distance(transform.position, traders[i].position) < distance)
			{
				distance = Vector3.Distance(transform.position, traders[i].position);
				trader = traders[i].GetComponent<CharacterStatus>();
			}
		}
		
		if(trader == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestTrade: No trader found.");
			return false;
		}

		if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestTrade: Selected trader " + trader.name + ".");

		// Get items to sell
		itemsToSell = new List<Item>();
		itemsToSell.AddRange(self.Inventory.Bag);

		// Get items to buy
		itemsToBuy = new List<Item>();
		itemsToBuy.AddRange(trader.Inventory.Bag);
		
		return true;
	}
	
	private void SendRequest()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestTrade: Requesting trade with \"" + trader.name + "\"; items to sell: " + itemsToSell.Count + ", items to buy: " + itemsToBuy.Count);

		CharacterUI.Log(this.name + " is requesting a trade with " + trader.name + ".");

		trade = trader.RequestTrade(self);
		if(trade != null)
		{
			CharacterUI.Log(trader.name + " has accepted the request to trade.");

			trade.OnOfferAccept += OfferAccepted;
			trade.OnOfferReject += OfferRejected;
			trade.Link(this);

			requestSent = true;
		}
	}

	private void Trade()
	{
		if(self.CanTrade() && trader.CanTrade())
		{
			// Sell all items from bag
			if(itemsToSell.Count > 0)
			{
				if(OnOffer != null)
				{
					offerOngoing = true;
					CharacterUI.Log(this.name + " is trying to sell " + itemsToSell[0].Name + " to " + trader.name + ".");
					OnOffer(ETradeType.Sell, itemsToSell[0]);
				}
			}
			// Upgrade equipment
			else if(itemsToBuy.Count > 0)
			{
				for(int i = 0; i < itemsToBuy.Count; i++)
				{
					if(self.CheckEquipment(itemsToBuy[i]) && self.Inventory.Gold >= itemsToBuy[i].Value)
					{
						if(OnOffer != null)
						{
							offerOngoing = true;
							CharacterUI.Log(this.name + " is trying to buy " + itemsToBuy[i].Name + " from " + trader.name + ".");
							OnOffer(ETradeType.Buy, itemsToBuy[i]);
						}

						break;
					}
				}

				if(!offerOngoing)
					AbortCurrentTrade();
			}
			// Abort trade
			else
				AbortCurrentTrade();
		}
		else
			AbortCurrentTrade();
	}

	private void OfferAccepted(ETradeType _Type, Item _Item)
	{
		offerOngoing = false;

		if(_Type == ETradeType.Sell)
		{
			itemsToSell.Remove(_Item);
			CharacterUI.Log(this.name + " has sold " + _Item.Name + " to " + trader.name + " for " + _Item.Value + " gold.");

			self.Inventory.Gold += _Item.Value;
			self.Inventory.Remove(_Item);

			trader.Inventory.Gold -= _Item.Value;
			trader.Inventory.Store(_Item);
		}
		else
		{
			itemsToBuy.Remove(_Item);
			CharacterUI.Log(this.name + " has bought " + _Item.Name + " from " + trader.name + " for " + _Item.Value + " gold.");

			trader.Inventory.Gold += _Item.Value;
			trader.Inventory.Remove(_Item);
			
			self.Inventory.Gold -= _Item.Value;
			self.PickUp(_Item);
		}
	}

	private void OfferRejected(ETradeType _Type, Item _Item)
	{
		offerOngoing = false;;

		if(_Type == ETradeType.Sell)
		{
			itemsToSell.Remove(_Item);
			CharacterUI.Log(trader.name + " has rejected to buy " + _Item.Name + " from " + this.name + " for " + _Item.Value + " gold.");
		}
		else
		{
			itemsToBuy.Remove(_Item);
			CharacterUI.Log(trader.name + " has rejected to sell " + _Item.Name + " to " + this.name + " for " + _Item.Value + " gold.");
		}
	}

	private void AbortCurrentTrade()
	{
		if(OnAbort != null)
		{
			CharacterUI.Log(this.name + " is aborting the trade with " + trader.name + ".");
			OnAbort();
		}

		// Update trader validity
		List<Detection> all = (List<Detection>)agent.Sensors.Memory[TargetTag];
		List<Detection> valid = (List<Detection>)agent.Sensors.Memory[PositiveTagPrefix + TargetTag];
		List<Detection> invalid = (List<Detection>)agent.Sensors.Memory[NegativeTagPrefix + TargetTag];

		// Find current trader
		Detection reference = null;
		for(int i = 0; i < all.Count; i++)
		{
			if(all[i].Reference == trader.gameObject)
			{
				reference = all[i];
				break;
			}
		}

		if(reference != null)
		{
			// Remove trader from valid list
			if(valid.Contains(reference))
				valid.Remove(reference);
			else if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestTrade: Unable to remove trader " + trader.name + " from valid trader list.");

			// Add trader to invalid list
			if(!invalid.Contains(reference))
				invalid.Add(reference);
			else if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestTrade: Unable to add trader " + trader.name + " to invalid trader list.");
		}
		else if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestTrade: Unable to locate trader " + trader.name + " in memory.");

		trader = null;
		trade = null;
		requestSent = false;
		inRange = false;
		offerOngoing = false;

		GetTrader();
	}
	
	#endregion
}

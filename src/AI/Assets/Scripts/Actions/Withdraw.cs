using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a withdrawing action.
/// </summary>
public class Withdraw : FollowPath 
{
	#region PUBLIC VARIABLES

	public bool ShowDirection = true;
	public string TargetTag = "Enemy";
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Vector3 withdrawDirection;

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	void OnDrawGizmos()
	{
		if(ShowDirection)
			Debug.DrawLine(transform.position, transform.position + withdrawDirection * 5.0f, Color.yellow);
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();
		
		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::Withdraw: Agent is lacking sensory system, no targets in sight.");
			return;
		}
		
		GetDirection();
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		GetDirection();
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellTo(transform.position, transform.position + withdrawDirection * 5.0f)));
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS

	private void GetDirection()
	{
		withdrawDirection = Vector3.zero;

		// Get all targets in sight
		//List<Transform> targets = (List<Transform>)agent.Sensors.Information[TargetTag];
		List<Vector3> targets = agent.Sensors.GetVectorList(TargetTag);
		
		if(targets == null || targets.Count == 0)
			return;

		// Get flight direction
		Vector3 dir;
		for(int i = 0; i < targets.Count; i++)
		{
			// Get flight direction
			dir = transform.position - targets[i];

			// Add it to the general direction
			withdrawDirection += dir;
		}

		withdrawDirection.Normalize();
	}

	#endregion
}

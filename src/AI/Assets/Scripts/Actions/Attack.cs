using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing an attack action.
/// </summary>
public class Attack : FollowPath 
{
	#region PUBLIC VARIABLES

	public bool ShowLogs = false;
	public string TargetTag = "Enemy";
	public float AttackRange = 1.0f;
	public float AttackSpeed = 0.5f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private CharacterStatus self;
	private CharacterStatus target;
	private bool inRange = false;
	private float attackCooldown = 0.0f;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	new protected void Start()
	{
		base.Start();

		// Get own character status
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Action::Attack: Unable to locate character status on game object, attack damage will be zero.");
	}

	void OnDrawGizmos()
	{
		if(target != null && TargetTag == "Enemy")
			Debug.DrawLine(transform.position, target.transform.position, Color.red);
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	override public void Prepare()
	{
		base.Prepare();

		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::Attack: Agent is lacking sensory system, no targets in sight.");
			return;
		}

		GetTarget();
	}
	
	override public void Execute()
	{
		// Cooldown attack timer
		attackCooldown += Time.deltaTime;

		if(target == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::Attack: No valid target, looking for new one.");
			if(!GetTarget())
				return;
		}

		if(inRange)
		{
			// Check if target is still in range
			if(Vector3.Distance(transform.position, target.transform.position) >= AttackRange)
			{
				if(ShowLogs)
					Debug.Log(this.name + " - Action::Attack: Target got out of range.");
				inRange = false;
			}
		}

		// Move to target
		if(!inRange)
			base.Execute();
		// Attempt to strike at target
		else
		{
			// Rotate towards target
			Vector3 direction = target.transform.position - transform.position;
			direction.y = 0.0f;
			direction.Normalize();
			transform.rotation = Quaternion.LookRotation(direction);

			// Hit target
			if(attackCooldown >= AttackSpeed)
				HitTarget();
		}
	}
	
	override public void CleanUp()
	{
		base.CleanUp();

		target = null;
		attackCooldown = 0.0f;
		inRange = false;
	}

	#endregion



	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellAt(transform.position, target.transform.position)));

		//return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(target.transform.position)));
	}
	
	override protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
			GridManager.Instance.OccupyCell(currentNode.Position);
		}

		if(Vector3.Distance(transform.position, target.transform.position) < AttackRange)
		{
			inRange = true;
		}
		else
		{
			GetTarget();
			GetNewPath();
			
			targetNode = tracker.GetNext();
			if(targetNode != null)
			{
				GridManager.Instance.OccupyCell(targetNode.Position);
				targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
			}
		}
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS

	private bool GetTarget()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::Attack: Getting new target.");

		// Get all targets in sight
		//List<Transform> targets = (List<Transform>)agent.Sensors.Information[TargetTag];

		// Get all targets in memory
		List<Detection> memory = (List<Detection>)agent.Sensors.Memory[TargetTag];
		// Sort memory from youngest to oldest
		memory.Sort();

		if(memory == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::Attack: No targets in sensory system.");
			return false;
		}
		
		// Get closest target
		float distance = float.PositiveInfinity;
		for(int i = 0; i < memory.Count; i++)
		{
			if(Vector3.Distance(transform.position, memory[i].Location) < distance)
			{
				distance = Vector3.Distance(transform.position, memory[i].Location);
				target = memory[i].Reference.GetComponent<CharacterStatus>();
			}
		}
		
		if(target == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::Attack: No target found.");
			return false;
		}

		return true;
	}

	private void HitTarget()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::Attack: Hitting target \"" + target.name + "\".");

		// Reset attack cooldown
		attackCooldown = 0.0f;

		// Damage target
		if(target.TakeDamage(self))
		{
			inRange = false;
			target = null;
		}
	}

	#endregion
}

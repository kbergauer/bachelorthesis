﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class Action : MonoBehaviour, IComparable
{
	#region PUBLIC VARIABLES

	public string Name = "Action";

	[SerializeField]
	public Requirement[] Requirements;

	[SerializeField]
	public Influence[] Influences;

	[SerializeField]
	public Personality Weights;

	public float UnpredictabilityVariance = 0.15f;
	public float UnpredictabilityMean = 1.0f;

	public float RawAffinity = 0.0f;
	public float CurrentAffinity = 0.0f;

	#endregion
	
	#region PROTECTED VARIABLES

	protected Agent agent;

	#endregion

	#region PRIVATE VARIABLES
	#endregion



	#region MONOBEHAVIOUR
	
	protected void Start()
	{
		// Get the agent component for later reference
		agent = GetComponent<Agent>();
		if(agent == null)
			Debug.Log(this.name + " - Action::" + Name + ": Unable to locate agent on game object, intended?");
	}

	#endregion



	#region PUBLIC METHODS

	/// <summary>
	/// Compares this instance with another object of the same type returning an integer indicating,
	/// whether this instance comes before or after the object in the sorting order or if they share 
	/// the same position.
	/// </summary>
	/// <returns>
	/// A negative value, if the object comes before this instance. 
	/// A positive value, if the object comes after this instance.
	/// Zero, if they share the same position in the sorting order.
	/// </returns>
	/// <param name="_Object">The object to compare to.</param>
	public int CompareTo(object _Object)
	{
		Action action = (Action)_Object;
		
		if(this.CurrentAffinity > action.CurrentAffinity)
			return -1;
		
		if(this.CurrentAffinity < action.CurrentAffinity)
			return 1;
		
		return 0;
	}

	/// <summary>
	/// Prepare the action to be executed.
	/// </summary>
	virtual public void Prepare()
	{
		//Debug.Log(this.name + ": Action::" + Name + ": Preparing.");
	}

	/// <summary>
	/// Executes the action.
	/// </summary>
	virtual public void Execute()
	{
		//Debug.Log(this.name + ": Action::" + Name + ": Executing.");
	}

	/// <summary>
	/// Cleans up the action after execution.
	/// </summary>
	virtual public void CleanUp()
	{
		//Debug.Log(this.name + ": Action::" + Name + ": Cleaning up.");
	}

	/// <summary>
	/// Checks all of the action's requirements.
	/// </summary>
	/// <returns><c>true</c>, if all requirements are met, <c>false</c> otherwise.</returns>
	/// <param name="_Info">The agent's memory.</param>
	public bool CheckRequirements(Hashtable _Info)
	{
		bool result = true;

		for(int i = 0; i < Requirements.Length; i++)
		{
			if(!Requirements[i].Check(_Info))
			{
				result = false;
				break;
			}
		}

		return result;
	}

	/// <summary>
	/// Gets the action's affinity.
	/// </summary>
	/// <returns>The affinity.</returns>
	/// <param name="_Info">The agent's memory.</param>
	public float GetAffinity(Hashtable _Info)
	{
		// Calculate fuzzy influence value
		float influence = 1.0f;
		if(Influences.Length == 1)
			influence = Influences[0].GetGain(_Info);
		else if(Influences.Length > 1)
		{
			// Fuzzy AND equation: m = min(mA, mB); Get smallest influence gain value
			float fuzzy = 1.0f;
			for(int i = 0; i < Influences.Length; i++)
			{
				fuzzy = Influences[i].GetGain(_Info);
				if(fuzzy < influence)
					influence = fuzzy;
			}
		}

		// Calculate final affinity: base affinity * fuzzy influence * random value to simulate human unpredicability
		return (RawAffinity * influence * (GetRandomNumber() * UnpredictabilityVariance + UnpredictabilityMean));
	}

	#endregion



	#region PRIVATE METHODS

	/// <summary>
	/// Gets a normally distributed random number.
	/// </summary>
	/// <returns>The random number.</returns>
	private float GetRandomNumber()
	{
		float random1 = Random.value;
		float random2 = Random.value;

		float result1 = Mathf.Cos(2.0f * Mathf.PI * random1) * Mathf.Sqrt(-2.0f * Mathf.Log(random2));
		float result2 = Mathf.Sin(2.0f * Mathf.PI * random1) * Mathf.Sqrt(-2.0f * Mathf.Log(random2));

		return result1;
	}

	#endregion
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a heal request action.
/// </summary>
public class RequestHealing : FollowPath 
{
	#region PUBLIC VARIABLES
	
	public bool ShowLogs = false;
	public string TargetTag = "Healer";
	public float RequestRange = 1.0f;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private CharacterStatus self;
	private HealerStatus healer;
	private bool inRange = false;
	private bool requestSent = false;

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	new protected void Start()
	{
		base.Start();
		
		// Get own character status
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Action::RequestHealing: Unable to locate character status on game object, attack damage will be zero.");
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();
		
		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::RequestHealing: Agent is lacking sensory system, no healers in sight.");
			return;
		}
		
		GetHealer();
	}
	
	override public void Execute()
	{
		if(healer == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestHealing: No valid healer, looking for new one.");
			if(!GetHealer())
				return;
		}
		
		if(inRange)
		{
			// Check if healer is still in range
			if(Vector3.Distance(transform.position, healer.transform.position) >= RequestRange)
			{
				if(ShowLogs)
					Debug.Log(this.name + " - Action::RequestHealing: Healer got out of range.");
				inRange = false;
			}
		}
		
		// Move to healer
		if(!inRange)
			base.Execute();
		else
		{
			// Rotate towards healer
			Vector3 direction = healer.transform.position - transform.position;
			direction.y = 0.0f;
			direction.Normalize();
			transform.rotation = Quaternion.LookRotation(direction);
			
			// Send request
			if(!requestSent)
				SendRequest();
		}
	}
	
	override public void CleanUp()
	{
		base.CleanUp();
		
		healer = null;
		inRange = false;
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellAt(transform.position, healer.transform.position)));
	}
	
	override protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
			GridManager.Instance.OccupyCell(currentNode.Position);
		}
		
		if(Vector3.Distance(transform.position, healer.transform.position) < RequestRange)
		{
			inRange = true;
		}
		else
		{
			if(!ValidatePath())
				GetNewPath();
			
			targetNode = tracker.GetNext();
			if(targetNode != null)
			{
				GridManager.Instance.OccupyCell(targetNode.Position);
				targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
			}
		}
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	
	private bool GetHealer()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestHealing: Getting new healer.");
		
		// Get all healers in sight
		//List<Transform> healers = (List<Transform>)agent.Sensors.Information[TargetTag];
		List<Transform> healers = agent.Sensors.GetTransformList(TargetTag);
		
		if(healers == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestHealing: No healers in sensory system.");
			return false;
		}
		
		// Get closest healer
		float distance = float.PositiveInfinity;
		for(int i = 0; i < healers.Count; i++)
		{
			if(Vector3.Distance(transform.position, healers[i].position) < distance)
			{
				distance = Vector3.Distance(transform.position, healers[i].position);
				healer = healers[i].GetComponent<HealerStatus>();
			}
		}
		
		if(healer == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::RequestHealing: No healer found.");
			return false;
		}
		
		return true;
	}
	
	private void SendRequest()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::RequestHealing: Requesting healing from \"" + healer.name + "\".");

		CharacterUI.Log(this.name + " is requesting healing from " + healer.name + ".");

		healer.RequestHeal(self);
		self.HealthAdjustment += HealthAdjusted;

		requestSent = true;
	}

	private void HealthAdjusted(float _Sign)
	{
		// Reset request once healing is received
		if(_Sign > 0.0f)
		{
			self.HealthAdjustment -= HealthAdjusted;

			healer = null;
			inRange = false;
			requestSent = false;
		}
	}
	
	#endregion
}

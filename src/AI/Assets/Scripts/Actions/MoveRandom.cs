using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a random movement action.
/// </summary>
[RequireComponent(typeof(PathTracker))]
public class MoveRandom : FollowPath 
{
	#region PUBLIC VARIABLES

	public float Range = 10.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion

	#region PRIVATE VARIABLES
	#endregion

	#region MONOBEHAVIOUR
	#endregion
	
	#region PUBLIC METHODS
	#endregion



	#region PROTECTED METHODS

	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetRandomGridCell(transform.position, (int)Range));
	}

	#endregion



	#region PRIVATE METHODS
	#endregion
}

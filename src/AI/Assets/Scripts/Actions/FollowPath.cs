﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a path following action.
/// </summary>
[RequireComponent(typeof(PathTracker))]
public class FollowPath : Action 
{
	#region PUBLIC VARIABLES
	
	public float Speed = 5.0f;
	public float Threshold = 1.0f;

	#endregion
	
	#region PROTECTED VARIABLES

	protected Vector3 targetPosition;
	protected Node targetNode;
	protected Node currentNode;
	protected PathTracker tracker;

	#endregion

	#region PRIVATE VARIABLES

	private Vector3 direction;

	#endregion

	#region MONOBEHAVIOUR

	void OnDrawGizmos()
	{
		Color old = Gizmos.color;

		/*
		if(targetNode != null)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawCube(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position)), 
			                new Vector3(GridManager.Instance.CellSize, GridManager.Instance.CellSize, GridManager.Instance.CellSize));
		}
		*/

		//Gizmos.color = Color.green;
		//Gizmos.DrawWireSphere(transform.position, Range);

		Gizmos.color = old;
	}

	#endregion
	
	#region PUBLIC METHODS

	override public void Prepare()
	{
		base.Prepare();

		if(tracker == null)
			tracker = GetComponent<PathTracker>();

		currentNode = new Node(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(transform.position)));
		GridManager.Instance.OccupyCell(currentNode.Position);
	}

	override public void Execute()
	{
		base.Execute();

		// Get the next node in the path
		if(targetNode != null && (transform.position - targetPosition).magnitude < Threshold)
			AdvancePath();

		// Get a new path
		if(targetNode == null)
		{
			bool path = false;
			path = GetNewPath();

			if(!path)
				return;

			AdvancePath();
		}

		Move();
	}

	override public void CleanUp()
	{
		base.CleanUp();

		if(GridManager.Instance != null)
		{
			if(currentNode != null)
				GridManager.Instance.FreeCell(currentNode.Position);
			if(targetNode != null)	
				GridManager.Instance.FreeCell(targetNode.Position);
		}

		targetNode = null;
	}

	#endregion



	#region PROTECTED METHODS

	protected bool ValidatePath()
	{
		int current = 0;

		while(current < tracker.PathArray.Count)
		{
			if(tracker.PathArray[current].IsObstacle)
				return false;

			current++;
		}

		return true;
	}

	virtual protected bool GetNewPath()
	{
		return false;
	}

	virtual protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
		}

		if(!ValidatePath())
			tracker.FindPath(tracker.GoalNode);
		
		targetNode = tracker.GetNext();
		if(targetNode != null)
		{
			GridManager.Instance.OccupyCell(targetNode.Position);
			targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
		}
	}

	#endregion



	#region PRIVATE METHODS

	private void Move()
	{
		// Get direction
		direction = targetPosition - transform.position;
		direction.y = 0.0f;
		direction.Normalize();
		//Debug.DrawRay(transform.position, direction * Threshold);
		
		// Rotate and move
		transform.rotation = Quaternion.LookRotation(direction);
		transform.position = transform.position + transform.forward * Speed * Time.deltaTime;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Influence 
{
	#region PUBLIC VARIABLES
	
	public enum InfoType
	{
		CollectionCount,
		Float,
		Integer
	}
	
	public string Tag = "Enemy";
	public InfoType Type = InfoType.CollectionCount;
	public float Maximum = 1.0f;
	public AnimationCurve GainCurve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 1.0f));
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	public float GetGain(Hashtable _Info)
	{
		return GainCurve.Evaluate(GetFuzzyValue(_Info));
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	
	private float GetFuzzyValue(Hashtable _Info)
	{
		float raw = GetRawValue(_Info);
		return raw / Maximum;
	}

	private float GetRawValue(Hashtable _Info)
	{
		float value = 0.0f;
		
		switch(Type)
		{
		case InfoType.CollectionCount:
		{
			value = GetCollectionCount(_Info[Tag]);
			if(value < 0.0f)
				value = 0.0f;
		} break;
			
		case InfoType.Float:
		case InfoType.Integer:
		{
			value = (float)_Info[Tag];
		} break;
		}
		
		return value;
	}
	
	private float GetCollectionCount(object _Info)
	{
		System.Collections.ICollection coll = _Info as System.Collections.ICollection;
		
		if(coll != null)
			return coll.Count;
		else
			return -1.0f;
	}
	
	#endregion
}

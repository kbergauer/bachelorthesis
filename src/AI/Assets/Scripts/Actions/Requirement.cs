﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Requirement 
{
	#region PUBLIC VARIABLES

	public enum InfoType
	{
		CollectionCount,
		Float,
		Integer
	}

	public enum ComparisonType
	{
		Equals,
		Greater,
		Less,
		GreaterEquals,
		LessEquals
	}

	public string Tag = "Enemy";
	public InfoType Type = InfoType.CollectionCount;
	public ComparisonType Comparison;
	public float Value;
	public bool Invert = false;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public bool Check(Hashtable _Info)
	{
		float valueToCheck = 0.0f;

		switch(Type)
		{
			case InfoType.CollectionCount:
				{
					valueToCheck = GetCollectionCount(_Info[Tag]);
					if(valueToCheck < 0.0f)
						return false;
				} break;

			case InfoType.Float:
				{
					valueToCheck = (float)_Info[Tag];
				} break;

			case InfoType.Integer:
				{
					valueToCheck = (float)((int)_Info[Tag]);
				} break;
		}

		if(Invert)
			return !Compare(valueToCheck);
		else
			return Compare(valueToCheck);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	private bool Compare(float _ValueToCheck)
	{
		bool result = false;

		switch(Comparison)
		{
			case ComparisonType.Equals:
				{
					result = (_ValueToCheck == Value);
				} break;
				
			case ComparisonType.Greater:
				{
					result = (_ValueToCheck > Value);
				} break;
				
			case ComparisonType.Less:
				{
					result = (_ValueToCheck < Value);
				} break;
				
			case ComparisonType.GreaterEquals:
				{
					result = (_ValueToCheck >= Value);
				} break;
				
			case ComparisonType.LessEquals:
				{
					result = (_ValueToCheck <= Value);
				} break;
		}

		return result;
	}

	private float GetCollectionCount(object _Info)
	{
		System.Collections.ICollection coll = _Info as System.Collections.ICollection;

		if(coll != null)
			return coll.Count;
		else
			return -1.0f;
	}

	#endregion
}

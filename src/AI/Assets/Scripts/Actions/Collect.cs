using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a collect action.
/// </summary>
public class Collect : FollowPath
{
	#region PUBLIC VARIABLES

	public Transform Treasure;
	public Transform DropOff;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private bool collected = false;
	private bool droppedOff = false;

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	override public void Prepare()
	{
		base.Prepare();

		if(Treasure == null && !collected)
			Debug.LogWarning(this.name + " - Action::Collect: No treasure to collect.");

		if(DropOff == null)
		{
			Debug.LogWarning(this.name + " - Action::Collect: No drop off specified, using starting position.");
			GameObject dropoff = new GameObject("DropOff");
			dropoff.transform.position = transform.position;
			DropOff = dropoff.transform;
		}
	}

	override public void Execute()
	{
		if(!droppedOff)
			base.Execute();
	}

	#endregion



	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		if(Treasure == null)
		{
			if(collected && DropOff != null)
			{
				if(GridManager.Instance.GetGridIndex(transform.position) != GridManager.Instance.GetGridIndex(DropOff.position))
					return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(DropOff.position)));
				else
				{
					droppedOff = true;
					CharacterUI.Log(this.name + " has dropped the collected item off at " + DropOff.name + ".");
					return false;
				}
			}
			else
				return false;
		}

		if(!collected)
		{
			// Get initial path to treasure
			if(GridManager.Instance.GetGridIndex(transform.position) != GridManager.Instance.GetGridIndex(Treasure.position))
				return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(Treasure.position)));
			// Get path to dropoff
			else
			{
				collected = true;
				CharacterUI.Log(this.name + " has collected " + Treasure.name + ".");
				Destroy(Treasure.gameObject);
				return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(DropOff.position)));
			}
		}

		return false;
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

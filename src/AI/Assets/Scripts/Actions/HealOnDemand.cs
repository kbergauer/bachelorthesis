using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a healing on demand action.
/// </summary>
public class HealOnDemand : FollowPath 
{
	#region PUBLIC VARIABLES
	
	public bool ShowLogs = false;
	public float HealRange = 1.0f;
	public float HealSpeed = 0.5f;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private HealerStatus self;
	private CharacterStatus target;
	private bool inRange = false;
	private float healCooldown = 0.0f;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	new protected void Start()
	{
		base.Start();
		
		// Get own character status
		self = GetComponent<HealerStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Action::HealOnDemand: Unable to locate healer status on game object, healing will be zero.");
	}
	
	void OnDrawGizmos()
	{
		if(target != null)
			Debug.DrawLine(transform.position, target.transform.position, Color.green);
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();
		
		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::HealOnDemand: Agent is lacking sensory system, no targets in sight.");
			return;
		}
	}
	
	override public void Execute()
	{
		// Cooldown heal timer
		healCooldown += Time.deltaTime;
		
		if(target == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::HealOnDemand: No valid target, looking for new one.");
			if(!GetTarget())
				return;
		}
		
		if(inRange)
		{
			// Check if target is still in range
			if(Vector3.Distance(transform.position, target.transform.position) >= HealRange)
			{
				if(ShowLogs)
					Debug.Log(this.name + " - Action::HealOnDemand: Target got out of range.");
				inRange = false;
			}
		}
		
		// Move to target
		if(!inRange)
			base.Execute();
		// Attempt to heal target
		else
		{
			// Rotate towards target
			Vector3 direction = target.transform.position - transform.position;
			direction.y = 0.0f;
			direction.Normalize();
			transform.rotation = Quaternion.LookRotation(direction);
			
			// Heal target
			if(healCooldown >= HealSpeed)
				HealTarget();
		}
	}
	
	override public void CleanUp()
	{
		base.CleanUp();
		
		target = null;
		healCooldown = 0.0f;
		inRange = false;
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellAt(transform.position, target.transform.position)));
		
		//return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(target.transform.position)));
	}
	
	override protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
			GridManager.Instance.OccupyCell(currentNode.Position);
		}
		
		if(Vector3.Distance(transform.position, target.transform.position) < HealRange)
		{
			inRange = true;
		}
		else
		{
			if(!ValidatePath())
				GetNewPath();
			
			targetNode = tracker.GetNext();
			if(targetNode != null)
			{
				GridManager.Instance.OccupyCell(targetNode.Position);
				targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
			}
		}
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	
	private bool GetTarget()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::HealOnDemand: Getting new target.");
		
		target = self.GetNextTarget();
		
		if(target == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::HealOnDemand: No target found.");
			return false;
		}
		
		return true;
	}
	
	private void HealTarget()
	{
		if(ShowLogs)
			Debug.Log(this.name + " - Action::HealOnDemand: Healing target \"" + target.name + "\".");
		
		// Reset attack cooldown
		healCooldown = 0.0f;
		
		// Heal target
		self.Heal(target);

		inRange = false;
		target = null;
	}
	
	#endregion
}

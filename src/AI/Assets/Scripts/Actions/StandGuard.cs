using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a standing guard action.
/// </summary>
public class StandGuard : FollowPath 
{
	#region PUBLIC VARIABLES
	
	public Transform Position;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();

		if(Position == null)
		{
			//Debug.LogWarning(this.name + " - Action::StandGuard: No guard position specified, using starting position.");
			GameObject guard = new GameObject(this.name + " Guard Position");
			guard.transform.position = transform.position;
			guard.transform.parent = transform.parent;
			transform.parent = guard.transform;
			Position = guard.transform;
		}
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		if(Position == null)
			return false;
		
		if(GridManager.Instance.GetGridIndex(transform.position) != GridManager.Instance.GetGridIndex(Position.position))
			return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(Position.position)));
		else
			return false;
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

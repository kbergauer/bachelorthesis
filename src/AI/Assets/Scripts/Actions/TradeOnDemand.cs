﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a trading on demand action.
/// </summary>
public class TradeOnDemand : FollowPath 
{
	#region EVENTS
	
	public event RequestTrade.OfferHandler OnOfferAccept;
	public event RequestTrade.OfferHandler OnOfferReject;
	
	#endregion

	#region PUBLIC VARIABLES
	
	public bool ShowLogs = false;
	public float TradeRange = 1.0f;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private CharacterStatus self;
	private CharacterStatus target;
	private RequestTrade trade;
	private bool inRange = false;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	new protected void Start()
	{
		base.Start();
		
		// Get own character status
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Action::TradeOnDemand: Unable to locate character status on game object, no trading possible.");
	}
	
	void OnDrawGizmos()
	{
		if(target != null)
			Debug.DrawLine(transform.position, target.transform.position, Color.green);
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	
	override public void Prepare()
	{
		base.Prepare();
		
		// Check sensory system
		if(agent.Sensors == null)
		{
			Debug.LogWarning(this.name + " - Action::TradeOnDemand: Agent is lacking sensory system, no targets in sight.");
			return;
		}
	}
	
	override public void Execute()
	{
		if(target == null)
		{
			if(ShowLogs)
				Debug.Log(this.name + " - Action::TradeOnDemand: No valid target.");

			return;
		}
		
		if(inRange)
		{
			// Check if target is still in range
			if(Vector3.Distance(transform.position, target.transform.position) >= TradeRange)
			{
				if(ShowLogs)
					Debug.Log(this.name + " - Action::TradeOnDemand: Target got out of range.");
				inRange = false;
			}
		}
		
		// Move to target
		if(!inRange)
			base.Execute();
		// Attempt to heal target
		else
		{
			// Rotate towards target
			Vector3 direction = target.transform.position - transform.position;
			direction.y = 0.0f;
			direction.Normalize();
			transform.rotation = Quaternion.LookRotation(direction);
		}
	}
	
	override public void CleanUp()
	{
		base.CleanUp();
		
		target = null;
		inRange = false;
	}

	public void Link(RequestTrade _Trade)
	{
		trade = _Trade;

		trade.OnOffer += HandleOffer;
		trade.OnAbort += HandleAbort;
	}

	#endregion
	
	
	
	#region PROTECTED METHODS
	
	override protected bool GetNewPath()
	{
		return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetClosestAdjacentCellAt(transform.position, target.transform.position)));
		
		//return tracker.FindPath(GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(target.transform.position)));
	}
	
	override protected void AdvancePath()
	{
		if(targetNode != null)
		{
			GridManager.Instance.FreeCell(currentNode.Position);
			currentNode = targetNode;
			GridManager.Instance.OccupyCell(currentNode.Position);
		}
		
		if(Vector3.Distance(transform.position, target.transform.position) < TradeRange)
		{
			inRange = true;
		}
		else
		{
			if(!ValidatePath())
				GetNewPath();
			
			targetNode = tracker.GetNext();
			if(targetNode != null)
			{
				GridManager.Instance.OccupyCell(targetNode.Position);
				targetPosition = GridManager.Instance.GetGridCellCenter(GridManager.Instance.GetGridIndex(targetNode.Position));
			}
		}
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS

	private void HandleOffer(RequestTrade.ETradeType _Type, Item _Item)
	{
		if(_Type == RequestTrade.ETradeType.Sell)
		{
			if(self.Inventory.Gold >= _Item.Value && OnOfferAccept != null)
				OnOfferAccept(_Type, _Item);
			else
				OnOfferReject(_Type, _Item);
		}
		else
			OnOfferAccept(_Type, _Item);
	}

	private void HandleAbort()
	{
		trade.OnOffer -= HandleOffer;
		trade.OnAbort -= HandleAbort;

		target = null;
		trade = null;

		inRange = false;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Class representing a sensory detection.
/// </summary>
[System.Serializable]
public class Detection : IComparable
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion

	#region PROPERTIES

	/// <summary>
	/// Gets and privately sets the age.
	/// </summary>
	/// <value>The age.</value>
	public float Age
	{
		get;
		private set;
	}

	/// <summary>
	/// Gets and privately sets the last known location.
	/// </summary>
	/// <value>The last known location.</value>
	public Vector3 Location
	{
		get;
		private set;
	}

	/// <summary>
	/// Gets and privately sets the reference.
	/// </summary>
	/// <value>The reference.</value>
	public GameObject Reference
	{
		get;
		private set;
	}

	#endregion
	
	
	
	#region CONSTRUCTORS

	/// <summary>
	/// Initializes a new instance of the <see cref="Detection"/> class.
	/// </summary>
	public Detection()
	{
		Age = 0.0f;
		Location = Vector3.zero;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Detection"/> class.
	/// </summary>
	/// <param name="_Reference">The reference.</param>
	public Detection(GameObject _Reference)
	{
		Age = 0.0f;
		Reference = _Reference;
		Location = _Reference.transform.position;
	}

	/// <summary>
	/// Releases unmanaged resources and performs other cleanup operations before the <see cref="Detection"/> is reclaimed
	/// by garbage collection.
	/// </summary>
	~Detection()
	{
		Reference = null;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Raises the instance's age.
	/// </summary>
	public void Mature()
	{
		Age += Time.deltaTime;
	}

	/// <summary>
	/// Updates the instance.
	/// </summary>
	public void Update()
	{
		Age = 0.0f;
		Location = Reference.transform.position;
	}

	/// <summary>
	/// Compares this instance with another object of the same type returning an integer indicating,
	/// whether this instance comes before or after the object in the sorting order or if they share 
	/// the same position.
	/// </summary>
	/// <returns>
	/// A negative value, if the object comes before this instance. 
	/// A positive value, if the object comes after this instance.
	/// Zero, if they share the same position in the sorting order.
	/// </returns>
	/// <param name="_Object">The object to compare to.</param>
	public int CompareTo(object _Object)
	{
		Detection detection = (Detection)_Object;
		
		if(this.Age < detection.Age)
			return -1;
		
		if(this.Age > detection.Age)
			return 1;
		
		return 0;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

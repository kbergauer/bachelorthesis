﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a proximity cognition.
/// </summary>
public class ProximitySense : Sense 
{
	#region PUBLIC VARIABLES
	
	public bool ShowTrackedObjects = true;
	public float Distance = 2;
	public float MemoryIntervall = 5.0f;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private List<Detection> detected;
	private Collider[] colliders;
	private Vector3 rayDirection;
	private RaycastHit hit;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	void OnDrawGizmos()
	{
		if(!ShowDebugInfo)
			return;

		Color old = Gizmos.color;
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, Distance);
		Gizmos.color = old;
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		// Prepare list of detected transforms
		detected = new List<Detection>();
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// Iterate all specified tags
		for(int i = 0; i < Tags.Length; i++)
		{
			int inTouch = 0;

			if(_Info.ContainsKey(Tags[i]))
			{
				detected = (List<Detection>)_Info[Tags[i]];
				
				// Update all entries and delete obsolete ones
				for(int d = 0; d < detected.Count; d++)
				{
					// Remove destroyed targets
					if(detected[d].Reference == null)
					{
						detected.RemoveAt(d);
						d--;
					}
					else
					{
						// Update entry's age
						detected[d].Mature();
						
						// Remove targets from memory after set intervall
						if(detected[d].Age >= MemoryIntervall)
						{
							detected.RemoveAt(d);
							d--;
						}
					}
				}
			}
			else
				detected = new List<Detection>();

			colliders = Physics.OverlapSphere(transform.position, Distance);
			
			for(int j = 0; j < colliders.Length; j++)
			{
				// Check for the correct tag
				if(colliders[j].tag == Tags[i])
				{
					// Update targets detection if it is already contained
					int index = detected.FindIndex(x => x.Reference == colliders[j].gameObject);
					if(index >= 0)
						detected[index].Update();
					else
						detected.Add(new Detection(colliders[j].gameObject));

					inTouch++;
				}
			}

			// Save gathered information to the specified memory
			if(!_Info.ContainsKey(Tags[i]))
				_Info.Add(Tags[i], detected);
			else
				_Info[Tags[i]] = detected;

			if(!_Info.ContainsKey(Tags[i] + "InTouch"))
				_Info.Add(Tags[i] + "InTouch", inTouch);
			else
				_Info[Tags[i] + "InTouch"] = inTouch;
		}
	}
	
	#endregion
	
	
	
	#region PROTECTED METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

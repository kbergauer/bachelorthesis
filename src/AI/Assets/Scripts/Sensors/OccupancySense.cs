﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sense observing the surrounding grid occupancy.
/// </summary>
public class OccupancySense : Sense 
{
	#region PUBLIC VARIABLES

	public int Range = 1;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private int cell = 0;
	private int row = 0;
	private int column = 0;

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		GetGridPosition();
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// Abort update if instance is outside of the grid
		if(!GridManager.Instance.IsInBounds(transform.position))
			return;

		// Get current position
		GetGridPosition();

		// Determine free cell count
		int total = (4 + ((Range - 1) * 2)) * Range;
		int free = 0;

		int span = 2 * Range;
		int limit = 0;
		int x = row - span / 2;
		int y = column;

		for(; x <= row + span / 2; x++)
		{
			y = column - limit;

			for(; y <= column + limit; y++)
			{
				if(GridManager.Instance.IsFree(x, y))
					free++;
			}

			if(x < row)
				limit++;
			else if(x >= row)
				limit--;
		}

		// Save information to specified memory
		if(_Info.ContainsKey("FreeCells"))
			_Info["FreeCells"] = free;
		else
			_Info.Add("FreeCells", free);

		if(_Info.ContainsKey("OccupiedCells"))
			_Info["OccupiedCells"] = total - free;
		else
			_Info.Add("OccupiedCells", total - free);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	private void GetGridPosition()
	{
		cell = GridManager.Instance.GetGridIndex(transform.position);
		row = GridManager.Instance.GetRow(cell);
		column = GridManager.Instance.GetColumn(cell);
	}

	#endregion
}

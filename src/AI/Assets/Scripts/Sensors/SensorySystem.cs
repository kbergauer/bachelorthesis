﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a simple sensory system.
/// </summary>
public class SensorySystem : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Sense[] senses;
	private Hashtable memory;

	#endregion

	#region PROPERTIES

	/// <summary>
	/// Returns the system's memory.
	/// </summary>
	/// <value>The memory.</value>
	public Hashtable Memory
	{
		get { return memory;	}
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		memory = new Hashtable();

		senses = GetComponents<Sense>();
		for(int i = 0; i < senses.Length; i++)
			senses[i].Initialize();

		//Debug.Log(this.name + " - SensorySystem: Identified " + senses.Length + " senses.");
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Gathers the information from all senses.
	/// </summary>
	public void GatherInformation()
	{
		// Gather new information from all senses
		for(int i = 0; i < senses.Length; i++)
			senses[i].UpdateSense(memory);
	}

	/// <summary>
	/// Returns a list of transforms corresponding to the given key from the system's memory.
	/// </summary>
	/// <returns>The transform list.</returns>
	/// <param name="_Key">The key.</param>
	public List<Transform> GetTransformList(string _Key)
	{
		List<Transform> result = null;

		if(memory.ContainsKey(_Key))
		{
			if(memory[_Key] is List<Detection>)
			{
				List<Detection> entry = (List<Detection>)memory[_Key];
				if(entry.Count > 0)
				{
					result = new List<Transform>();
					for(int i = 0; i < entry.Count; i++)
						result.Add(entry[i].Reference.transform);
				}
			}
			else
				result = (List<Transform>)memory[_Key];
		}

		return result;
	}

	/// <summary>
	/// Returns a list of vectors corresponding to the given key from the system's memory.
	/// </summary>
	/// <returns>The vector list.</returns>
	/// <param name="_Key">The key.</param>
	public List<Vector3> GetVectorList(string _Key)
	{
		List<Vector3> result = new List<Vector3>();
		
		if(memory.ContainsKey(_Key))
		{
			if(memory[_Key] is List<Detection>)
			{
				List<Detection> entry = (List<Detection>)memory[_Key];
				for(int i = 0; i < entry.Count; i++)
					result.Add(entry[i].Location);
			}
			else
				result = (List<Vector3>)memory[_Key];
		}
		
		return result;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

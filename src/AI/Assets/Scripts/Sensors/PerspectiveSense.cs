﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class representing a perspective vision sense.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class PerspectiveSense : Sense 
{
	#region PUBLIC VARIABLES

	public bool ShowTrackedObjects = true;
	public float FieldOfView = 45;
	public float ViewDistance = 10;
	public float MemoryIntervall = 5.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private List<Detection> detected;
	private Transform[][] targets;
	private Vector3 rayDirection;
	private RaycastHit hit;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void OnDrawGizmos()
	{
		if(!ShowDebugInfo)
			return;

		if(ShowTrackedObjects && targets != null)
			for(int i = 0; i < targets.Length; i++)
				for(int j = 0; j < targets[i].Length; j++)
					if(targets[i][j] != null)
						Debug.DrawLine(transform.position, targets[i][j].position, Color.red);

		Vector3 frontRayPoint = transform.position + (transform.forward * ViewDistance);
		Vector3 leftRayPoint = transform.position + Quaternion.Euler(0.0f, FieldOfView * 0.5f, 0.0f) * (transform.forward * ViewDistance);
		Vector3 rightRayPoint = transform.position + Quaternion.Euler(0.0f, -FieldOfView * 0.5f, 0.0f) * (transform.forward * ViewDistance);

		Debug.DrawLine(transform.position, frontRayPoint, Color.green);
		Debug.DrawLine(transform.position, leftRayPoint, Color.green);
		Debug.DrawLine(transform.position, rightRayPoint, Color.green);

		Debug.DrawLine(leftRayPoint, frontRayPoint, Color.green);
		Debug.DrawLine(rightRayPoint, frontRayPoint, Color.green);
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		// Prepare list of detected transforms
		detected = new List<Detection>();

		targets = new Transform[Tags.Length][];
		for(int i = 0; i < Tags.Length; i++)
		{
			// Get targets to sense
			GameObject[] objects = GameObject.FindGameObjectsWithTag(Tags[i]);
			targets[i] = new Transform[objects.Length];
			for(int j = 0; j < objects.Length; j++)
				targets[i][j] = objects[j].transform;
		}
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// Abort if no targets are specified
		if(targets == null || targets.Length == 0)
			return;

		// Iterate all specified tags
		for(int i = 0; i < Tags.Length; i++)
		{
			if(targets[i] == null || targets[i].Length == 0)
				continue;

			if(_Info.ContainsKey(Tags[i]))
			{
				detected = (List<Detection>)_Info[Tags[i]];
				
				// Update all entries and delete obsolete ones
				for(int d = 0; d < detected.Count; d++)
				{
					// Remove destroyed targets
					if(detected[d].Reference == null)
					{
						detected.RemoveAt(d);
						d--;
					}
					else
					{
						// Update entry's age
						detected[d].Mature();

						// Remove targets from memory after set intervall
						if(detected[d].Age >= MemoryIntervall)
						{
							detected.RemoveAt(d);
							d--;
						}
					}
				}
			}
			else
				detected = new List<Detection>();

			int inSight = 0;
			
			for(int j = 0; j < targets[i].Length; j++)
			{
				// Check target validity
				if(targets[i][j] == null)
					continue;
				
				// Get direction to target
				rayDirection = targets[i][j].position - transform.position;
				
				// Check if the target is inside the field of view
				if(Vector3.Angle(rayDirection, transform.forward) < FieldOfView * 0.5f)
				{
					// Check if the target is in view distance
					if(Physics.Raycast(transform.position, rayDirection, out hit, ViewDistance))
					{
						// Check for the correct target and tag
						if(hit.transform == targets[i][j] && hit.collider.tag == Tags[i])
						{
							// Update targets detection if it is already contained
							int index = detected.FindIndex(x => x.Reference == targets[i][j].gameObject);
							if(index >= 0)
								detected[index].Update();
							else
								detected.Add(new Detection(targets[i][j].gameObject));

							inSight++;
						}
					}
				}
			}

			// Save gathered information to the specified memory
			if(!_Info.ContainsKey(Tags[i]))
				_Info.Add(Tags[i], detected);
			else
				_Info[Tags[i]] = detected;

			if(!_Info.ContainsKey(Tags[i] + "InSight"))
				_Info.Add(Tags[i] + "InSight", inSight);
			else
				_Info[Tags[i] + "InSight"] = inSight;
		}
	}

	#endregion



	#region PROTECTED METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

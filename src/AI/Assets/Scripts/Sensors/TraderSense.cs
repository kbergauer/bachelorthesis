﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Sense observing trader availability.
/// </summary>
public class TraderSense : Sense 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private CharacterStatus self;

	private List<Detection> all;
	private List<Detection> valid;
	private List<Detection> invalid;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Sense::HealthSense: Unable to locate character status, cannot track health.");
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// No traders to update
		if(!_Info.ContainsKey(Tags[0]))
			return;

		// Get all traders
		all = (List<Detection>)_Info[Tags[0]];

		// Get previously valid traders
		if(_Info.ContainsKey(Tags[1]))
		{
			valid = (List<Detection>)_Info[Tags[1]];

			// Removed destroyed instances
			for(int i = 0; i < valid.Count; i++)
			{
				if(!all.Contains(valid[i]))
				{
					valid.RemoveAt(i);
					i--;
				}
			}
		}
		else
			valid = new List<Detection>();

		// Get previously invalid traders
		if(_Info.ContainsKey(Tags[2]))
		{
			invalid = (List<Detection>)_Info[Tags[2]];

			// Removed destroyed instances
			for(int i = 0; i < invalid.Count; i++)
			{
				if(!all.Contains(invalid[i]))
				{
					invalid.RemoveAt(i);
					i--;
				}
			}
		}
		else
			invalid = new List<Detection>();

		// Add new traders to valid list
		if(all.Count > valid.Count + invalid.Count)
		{
			for(int i = 0; i < all.Count; i++)
			{
				if(!invalid.Contains(all[i]) && !valid.Contains(all[i]))
					valid.Add(all[i]);
			}
		}

		// Update valid traders
		if(!_Info.ContainsKey(Tags[1]))
			_Info.Add(Tags[1], valid);
		else
			_Info[Tags[1]] = valid;

		// Update invalid traders
		if(!_Info.ContainsKey(Tags[2]))
			_Info.Add(Tags[2], invalid);
		else
			_Info[Tags[2]] = invalid;
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a sense.
/// Implemented according to [KyPS2013]: Unity 4.x Game AI Programming
/// </summary>
public class Sense : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public bool ShowDebugInfo = true;
	public string[] Tags;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	virtual public void Initialize()
	{
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	virtual public void UpdateSense(Hashtable _Info)
	{
	}

	#endregion



	#region PROTECTED METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sense observing character related data.
/// </summary>
public class StatusSense : Sense 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private CharacterStatus self;

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Sense::StatusSense: Unable to locate character status component, cannot track status.");
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// Initialize default values
		float health = 0.0f;
		float power = 0.0f;

		// Gather information
		if(self != null)
		{
			health = self.CurrentHealth;
			power = self.PowerLevel;
		}

		// Save information to the specified memory
		if(_Info.ContainsKey("Health"))
			_Info["Health"] = health;
		else
			_Info.Add("Health", health);

		if(_Info.ContainsKey("PowerLevel"))
			_Info["PowerLevel"] = power;
		else
			_Info.Add("PowerLevel", power);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

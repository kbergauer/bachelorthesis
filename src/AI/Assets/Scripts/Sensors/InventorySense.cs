﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sense observing inventory related data.
/// </summary>
public class InventorySense : Sense 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private CharacterStatus self;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	/// <summary>
	/// Initializes the sense.
	/// </summary>
	override public void Initialize()
	{
		self = GetComponent<CharacterStatus>();
		if(self == null)
			Debug.LogWarning(this.name + " - Sense::HealthSense: Unable to locate character status, cannot track health.");
	}

	/// <summary>
	/// Updates the sense saving all new information into the given hashtable.
	/// </summary>
	/// <param name="_Info">The sensory system's memory.</param>
	override public void UpdateSense(Hashtable _Info)
	{
		// Setup default values
		int gold = 0;
		bool hasWeapon = false;
		bool hasArmor = false;
		int itemsInBag = 0;
		float volume = 0.0f;

		// Gather information from inventory
		if(self != null)
		{
			gold = self.Inventory.Gold;
			hasWeapon = self.Inventory.Weapon != null;
			hasArmor = self.Inventory.Armor != null;
			itemsInBag = self.Inventory.Bag.Count;
			volume = self.Inventory.CurrentVolume;
		}

		// Save information to specified memory
		if(_Info.ContainsKey("Gold"))
			_Info["Gold"] = gold;
		else
			_Info.Add("Gold", gold);

		if(_Info.ContainsKey("HasWeapon"))
			_Info["HasWeapon"] = hasWeapon;
		else
			_Info.Add("HasWeapon", hasWeapon);

		if(_Info.ContainsKey("HasArmor"))
			_Info["HasArmor"] = hasArmor;
		else
			_Info.Add("HasArmor", hasArmor);

		if(_Info.ContainsKey("ItemsInBag"))
			_Info["ItemsInBag"] = itemsInBag;
		else
			_Info.Add("ItemsInBag", itemsInBag);

		if(_Info.ContainsKey("Volume"))
			_Info["Volume"] = volume;
		else
			_Info.Add("Volume", volume);
	}
	
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public enum EMovementType
	{
		Static,
		Free,
		Follow
	};

	public EMovementType Movement = EMovementType.Static;
	public Vector3 StaticPosition = Vector3.zero;
	public float Speed = 1.0f;
	public Transform FollowTarget = null;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Vector3 position = Vector3.zero;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Update()
	{
		switch(Movement)
		{
		case EMovementType.Static:
		{
			position = StaticPosition;
		} break;

		case EMovementType.Free:
		{
			if(Input.GetMouseButton(1))
			{
				position = position + new Vector3(Input.mousePosition.x - Screen.width / 2, 0.0f, Input.mousePosition.y - Screen.height / 2) * Speed * Time.deltaTime;
			}
		} break;

		case EMovementType.Follow:
		{
			position = FollowTarget.transform.position;
			position.y = Camera.main.transform.position.y;
			position.z -= 7.5f;
		} break;
		}

		Camera.main.transform.position = position;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}
